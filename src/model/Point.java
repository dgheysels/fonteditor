/*
 * Created on 29-mrt-2005
 */
package model;

import java.awt.geom.Point2D;

public class Point extends Point2D.Double implements Cloneable {

    private int     _id;
	private boolean _oncurve;
		
	public Point() {}
	public Point(int id, int x, int y, boolean oncurve) {
		super(x, y);
		_id = id;
		_oncurve = oncurve;
	}
	
	public int getID() { return _id; }
	public boolean isOncurve() { return _oncurve; }
	
	public void setID(int id) { _id = id; }
	public void setOncurve(boolean oncurve) { _oncurve = oncurve; }
		
	public void translate(int dx, int dy) {
		x += dx;
		y += dy;
	}
	
	public String toString() {
		return 	"(" + _id + ") " +
				((_oncurve) ? "[onCurve] " : "[       ] ") + 
				getX() + ":" + getY();		   
	}
	    	
	public Object clone() {
		return super.clone();
	}
}

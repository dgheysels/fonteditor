/*
 * Created on 29-mrt-2005
 */
package model;

import java.util.*;

public class Contour implements Cloneable {
	
	private Vector _points = new Vector();
	
	public Contour() {}
	
	public void translate(int dx, int dy) {
		Iterator it = _points.iterator();
		
		while (it.hasNext()) {
			((Point)it.next()).translate(dx, dy);
		}	
	}
	
	public void scale(double xScale, double yScale, int arg1, int arg2) {
		Iterator it = _points.iterator();
		Point    currentPoint;
		
		while (it.hasNext()) {
			currentPoint = (Point)it.next();
			currentPoint.setLocation(arg1 + (xScale * currentPoint.getX()),
									 arg2 + (yScale * currentPoint.getY()));
		}			
	}
	
	public void scale2by2(double scale10, double scale01, int arg1, int arg2) {
		Iterator it = _points.iterator();
		Point    currentPoint;
		
		while (it.hasNext()) {
			currentPoint = (Point)it.next();
			currentPoint.setLocation(arg1 + (scale10 * currentPoint.getY()),
									 arg2 + (scale01 * currentPoint.getX()));
		}			
	}
	
	public void addPoint(Point p) {
		_points.add(p);
	}
	
	public void removePoint(Point p) {
		_points.remove(p);
	}
	
	public Vector getPoints() {
		return _points;
	}
	
	public void setPoints(Vector p) {
	    _points = p;
	}
	
	public String toString() {
		
		String s = new String();
		Iterator it = _points.iterator();
		
		while (it.hasNext()) {
			s += "\t" + it.next() + "\n"; 
		}
		
		return s + "\n";
		
	}
	
	public Object clone() throws CloneNotSupportedException {
		Contour clonedContour = (Contour)super.clone();
		Vector  points        = new Vector();
		
		Iterator it = _points.iterator();
		
		while (it.hasNext()) {
		    points.add(((model.Point)it.next()).clone());
		}
				
		clonedContour.setPoints(points);
		
		return clonedContour;
	}
}

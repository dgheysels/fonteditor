/*
 * Created on 29-mrt-2005
 */
package model;

import java.util.*;

/**
 * @author Dimitri Gheysels
 */
public abstract class Glyph implements Cloneable {

	private int   _id;
	private short _xMin;
	private short _yMin;
	private short _xMax;
	private short _yMax;

	private boolean _modified = false;
	
	
	public abstract void translate(int dx, int dy);
	public abstract void scale(double xScale, double yScale, int arg1, int arg2);
	public abstract void scale2by2(double scale10, double scale01, int arg1, int arg2);
	
	public void   addGlyphComponent(Glyph g) {}
	
	public Vector getGlyphComponents() { return null; }
	
	public abstract Vector getContours();
	public abstract void   addContour(Contour c);
	public  		int    getTotalContours()  {
	    return getContours().size();
	}
	
	public abstract Vector getPoints();
	public abstract Point  getPoint(int x, int y, int precision);
	public abstract void   addPoint(Point newPoint);
	public abstract void   removePoint(Point p);
	public 			int    getTotalPoints() {
	    return getPoints().size();
	}
	
	public int   getID() { return _id; }
	public short getXMin() { return _xMin; }
	public short getXMax() { return _yMin; }
	public short getYMin() { return _xMax; }
	public short getYMax() { return _yMax; }
	public boolean isModified() { return _modified; }
	
	public void setID(int id) { _id = id; }
	public void setXMin(short xMin) { _xMin = xMin; }
	public void setXMax(short yMin) { _yMin = yMin; }
	public void setYMin(short xMax) { _xMax = xMax; }
	public void setYMax(short yMax) { _yMax = yMax; }
	public void setModified(boolean modified) { _modified = modified; }
	
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
	
	public abstract String getType();
}

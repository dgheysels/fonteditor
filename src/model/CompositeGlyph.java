/*
 * Created on 29-mrt-2005
 */
package model;

import java.util.*;

public class CompositeGlyph extends Glyph {

	private Vector _glyphComponents = new Vector();
	
	public void translate(int dx, int dy) {	
		Iterator it = _glyphComponents.iterator();
		
		while (it.hasNext()) {
			((Glyph)it.next()).translate(dx, dy);
		}	
	}
	
	public void scale(double xScale, double yScale, int arg1, int arg2) {
		Iterator it = _glyphComponents.iterator();
		
		while (it.hasNext()) {
			((Glyph)it.next()).scale(xScale, yScale, arg1, arg2);
		}	
	}
	
	public void scale2by2(double scale10, double scale01, int arg1, int arg2) {
		Iterator it = _glyphComponents.iterator();
		
		while (it.hasNext()) {
			((Glyph)it.next()).scale2by2(scale10, scale01, arg1, arg2);
		}	
	}
		
	public Vector getGlyphComponents() { return _glyphComponents; }
	
	public void addGlyphComponent(Glyph g) {
		_glyphComponents.add(g);
	}
	
	public Vector getPoints() {   
	    Vector points   = new Vector();
	    Iterator it     = _glyphComponents.iterator();//contours.iterator();
	    
	    while (it.hasNext()) {
	        points.addAll(((Glyph)it.next()).getPoints());
	    }
	    
	    return points; 
	}
	
	public Point  getPoint(int x, int y, int precision) { 
	    Iterator it = _glyphComponents.iterator();
	    Point foundPoint = null;
	    
	    // vanaf we een punt gevonden hebben, stoppen we met zoeken
	    while (it.hasNext() && foundPoint == null) {
	        foundPoint = ((Glyph)it.next()).getPoint(x, y, precision);
	    }
	    
	    return foundPoint;   
	}
	
	public void addPoint(Point newPoint) {
	    Iterator it = _glyphComponents.iterator();
	    
	    while (it.hasNext()) {
	        ((Glyph)it.next()).addPoint(newPoint);
	    }   
	}
	
	public void removePoint(Point p) {}
	
	public Vector getContours() {
		
		Iterator it = _glyphComponents.iterator();
		Vector   v  = new Vector();
		
		while (it.hasNext()) {
			v.addAll(((Glyph)it.next()).getContours());
		}
		
		return v;	
	}
	
	public void   addContour(Contour c) {}
	
	public String toString() {
		
		String s = new String();
		Iterator it = _glyphComponents.iterator();
		
		while (it.hasNext()) {
			s += it.next();
		}
		
		return "+COMPOSITE GLYPH\n" + s;
		
	}
	
	public Object clone() throws CloneNotSupportedException {		
		return super.clone();
	}
	
	public String getType() { return "composite"; }	
}

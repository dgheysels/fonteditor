/*
 * Created on 25-apr-2005
 */
package model;

import java.io.IOException;
import java.util.*;

import datalayer.FontManager;

public class FontModel extends Observable {

    private boolean         _fontloaded;
    
    private FontManager     _fontmanager;
    private Glyph[] 	    _glyphs;
    private FontInformation _fontinfo;
     
    public FontModel() {
        _fontmanager = FontManager.getInstance(); 
        _fontloaded  = false;
    }

    public void load(String filename) throws IOException {
        _fontmanager.setFontname(filename);
        _fontmanager.loadFont();
        
        setFontInformation(_fontmanager.getFontInformation());
        setGlyphs(_fontmanager.getAllGlyphs());
       
        _fontloaded = true;
    }
    
    public void save() throws IOException {
       
        for (int i=0; i<_glyphs.length; i++) {
	        if (_glyphs[i] != null && _glyphs[i].isModified()) {
	            _fontmanager.setGlyph(_glyphs[i]);
	        	_glyphs[i].setModified(false);
	        }
	    }
	   	   
	    _fontmanager.saveFont(); 
    }
    
    public void save(String filename) throws IOException {
        
        _fontmanager.setFontname(filename);
        save();
    }
    
    public void close() throws IOException {
        _fontmanager.close();
        _fontloaded = false;
    }
    
    public boolean isFontLoaded() {
        return _fontloaded;
    }
    
    public FontInformation getFontInformation() { 
        return _fontinfo; 
    }
    
    public Glyph[] getGlyphs() {
        return _glyphs; 
    }
    
    public void setFontInformation(FontInformation fontinfo) { 
    
        _fontinfo = fontinfo;
        
        setChanged();
        notifyObservers();   
    }
    
    public void setGlyphs(Glyph[] glyphs) { 
        _glyphs = glyphs;
    }
    
    public void setGlyph(Glyph glyph){

        _glyphs[glyph.getID()] = glyph;
        
        setChanged();
        notifyObservers(new Integer(glyph.getID()));     
    }
}

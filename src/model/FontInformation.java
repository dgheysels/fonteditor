/*
 * Created on 31-mrt-2005
 */
package model;

import java.util.*;

/**
 * @author Dimitri Gheysels
 */
public class FontInformation {

	// NAME table
	private String _fontVersion;
	private String _copyright;
	private String _fontFamily;
	private String _fontName;
	private String _trademark;
	private String _manufacturerName;
	private String _designerName;
	private String _typefaceDescription;
	private String _vendorURL;
	private String _designerURL;
	private String _licenceDescription;
	private String _licenceURL;
	
	// HEAD table
	private Date   _created;
	private Date   _modified;
	private int    _UnitsPerEM;
	private short    _xMin;
	private short    _yMin;
	private short    _xMax;
	private short    _yMax;
		
	// MAXP table
	private int    _numberOfGlyphs;
	
	public String getCopyright() { return _copyright; }
	public Date getCreated() { return _created; }
	public String getDesignerName() { return _designerName; }
	public String getDesignerURL() { return _designerURL; }
	public String getFontFamily() { return _fontFamily; }
	public String getFontName() { return _fontName; }
	public String getFontVersion() { return _fontVersion; }
	public String getLicenceDescription() { return _licenceDescription; }
	public String getLicenceURL() { return _licenceURL; }
	public String getManufacturerName() { return _manufacturerName; }
	public Date getModified() { return _modified; }
	public int getNumberOfGlyphs() { return _numberOfGlyphs; }
	public String getTrademark() { return _trademark; }
	public String getTypefaceDescription() { return _typefaceDescription; }
	public int getUnitsPerEM() { return _UnitsPerEM; }
	public String getVendorURL() { return _vendorURL; }
	public short getXMin() { return _xMin; }
	public short getYMin() { return _yMin; }
	public short getXMax() { return _xMax; }
	public short getYMax() { return _yMax; }
	
	public void setCopyright(String copyright) { _copyright = copyright; }
	public void setCreated(Date created) { _created = created; }
	public void setDesignerName(String name) { _designerName = name; }
	public void setDesignerURL(String designerURL) { _designerURL = designerURL; }
	public void setFontFamily(String fontFamily) { _fontFamily = fontFamily; }
	public void setFontName(String fontName) { _fontName = fontName; }
	public void setFontVersion(String version) { _fontVersion = version; }
	public void setLicenceDescription(String description) { _licenceDescription = description; }
	public void setLicenceURL(String licenceURL) { _licenceURL = licenceURL; }
	public void setManufacturerName(String name) {	_manufacturerName = name; }
	public void setModified(Date modified) { _modified = modified; }
	public void setNumberOfGlyphs(int numberOfGlyphs) { _numberOfGlyphs = numberOfGlyphs; }
	public void setTrademark(String trademark) { _trademark = trademark; }
	public void setTypefaceDescription(String description) { _typefaceDescription = description; }
	public void setUnitsPerEM(int unitsPerEM) { _UnitsPerEM = unitsPerEM; }
	public void setVendorURL(String vendorURL) { _vendorURL = vendorURL; }
	public void setXMin(short xMin) { _xMin = xMin; }
	public void setYMin(short yMin) { _yMin = yMin; }
	public void setXMax(short xMax) { _xMax = xMax; }
	public void setYMax(short yMax) { _yMax = yMax; }
}

/*
 * Created on 29-mrt-2005
 */
package model;

import java.util.*;

public class SimpleGlyph extends Glyph {

	private Vector _contours = new Vector();
	
	public void translate(int dx, int dy) {
		Iterator it = _contours.iterator();
		
		while (it.hasNext()) {
			((Contour)it.next()).translate(dx, dy);
		}
	}
	
	public void scale(double xScale, double yScale, int arg1, int arg2) {
		Iterator it = _contours.iterator();
		
		while (it.hasNext()) {
			((Contour)it.next()).scale(xScale, yScale, arg1, arg2);
		}	
	}
	
	public void scale2by2(double scale10, double scale01, int arg1, int arg2) {
		Iterator it = _contours.iterator();
		
		while (it.hasNext()) {
			((Contour)it.next()).scale2by2(scale10, scale01, arg1, arg2);
		}
	}

	public Point getPoint(int x, int y, int precision) {
	    Vector points = getPoints();
	    Iterator it  = points.iterator();
	    
	    Point  currentPoint;
	    Point  foundPoint = null;
	    while (it.hasNext()) {
			currentPoint = ((model.Point)it.next());
			
			if ((x >= currentPoint.getX() - precision && x <= currentPoint.getX() + precision)  && 
				(y >= currentPoint.getY() - precision && y <= currentPoint.getY() + precision)) {
				
				// referentie naar het geselecteerde punt
			    foundPoint = currentPoint;
			}			
		}
	    
	    return foundPoint;   
	}
	
	/*
	 * Alle punten overlopen en hun ID's verhogen totdat we het punt tegenkomen met
	 * ID < newPoint.ID, beginnen met het laatste punt v/d laatste contour. 
	 * 
	 * vb: newPoint.ID = 1
	 * 0 | 1 | 2 | 3 | 4
	 *   ^				
	 * 0 | 2 | 3 | 4 | 5     -> de ID's verhogen totdat currentPoint.ID < newPoint.ID 
	 * ^                     -> currentPoint
	 * 
	 * 0 | 2 | 3 | 4 | 5  
	 *     ^                 -> itPoint.next()
	 * 
	 * 0 | 1 | 2 | 3 | 4 | 5 -> newPoint wordt net voor currentPoint toegevoegd
	 *  
	 * Het beginpunt v/d contour kan dus nooit gewijzigd worden
	 * 
	 */
	public void addPoint(Point newPoint) {
	    Vector contours = getContours();
	    Vector points;
	    ListIterator contourIt = contours.listIterator(contours.size());
	    ListIterator pointIt;
	    	    
	    boolean done = false;
	    Point currentPoint;
	    
	    while (contourIt.hasPrevious() && !done) {
	        points = ((Contour)contourIt.previous()).getPoints();
	        pointIt = points.listIterator(points.size());
	        
	        while (pointIt.hasPrevious() && !done) {
	            currentPoint = (Point)pointIt.previous();
	            
	            // als currentPoint.ID  < newPoint.ID wil dat zeggen dat we newPoint na
	            // currentPoint moeten toevoegen (->pointIt.next)
	            if (currentPoint.getID() < newPoint.getID()) {
	                pointIt.next();
	                pointIt.add(newPoint);
	                done = true;
	            }
	            else {
	                currentPoint.setID(currentPoint.getID()+1);
	            }
	        }
	    }   
	}

	public void removePoint(Point p) {
	    
	    Vector contours = getContours();
	    Vector points;
	    ListIterator contourIt = contours.listIterator(getTotalContours());
	    ListIterator pointIt;
	   
	    Point currentPoint;
	    
	    boolean done = false;   
	    
	    while (contourIt.hasPrevious() && !done) {
	      
	        points = ((Contour)contourIt.previous()).getPoints();
	        pointIt = points.listIterator(points.size());
	        
	        while (pointIt.hasPrevious() && !done) {
	            currentPoint = (Point)pointIt.previous();
	            
	            // als currentPoint.ID == p.ID moeten we currentPoint verwijderen
	            if (currentPoint.getID() == p.getID()) {        	                
	                pointIt.remove();
	                done = true;	                
	            }
	            else {
	                currentPoint.setID(currentPoint.getID()-1);
	            }
	        }
	    }      
	}
		
	public Vector getPoints() {
	    Vector   points = new Vector();
	    Iterator it     = _contours.iterator();
	    
	    while (it.hasNext()) {
	        points.addAll(((Contour)it.next()).getPoints());
	    }
	    
	    return points;
	}
	
	public void   addContour(Contour c) { 
	    _contours.add(c); 
	}
	
	public Vector getContours() { 
	    return _contours;
	}
	
	public void setContours(Vector contours) {
	    _contours = contours; 
	}
	
	public String toString() {
		
		String s = new String();
		Iterator it = _contours.iterator();
		
		while (it.hasNext()) {
			s += it.next();
		}
		
		return "  +SIMPLE GLYPH\n" + s;
	}
	
	public Object clone() throws CloneNotSupportedException {
		
	    SimpleGlyph clonedGlyph = (SimpleGlyph)super.clone();
		Vector      contours    = new Vector();
				
		Iterator it = _contours.iterator();
		
		while (it.hasNext()) {
		    contours.add(((Contour)it.next()).clone());
		}
		
		clonedGlyph.setContours(contours);
		
		return clonedGlyph;		
	}
	
	public String getType() { return "simple"; }	
}

/*
 * Created on 14-apr-2005
 */
package gui;

import java.awt.event.*;
import javax.swing.*;

import model.*;
import gui.glyphFrameFactory.*;

public class GlyphTableFrame extends JInternalFrame implements IGUIConstants {

	private JTable 		_glyphTable;
	private JScrollPane _scrollpane;
	private JPanel      _panel;	
	private FontModel   _fontmodel;
		
	public GlyphTableFrame(String title, FontModel fontmodel) {
		super(title, true, true, true, true);	
		_fontmodel = fontmodel;
				
		setDefaultCloseOperation(JInternalFrame.HIDE_ON_CLOSE);
		initGUI();	
	}
	
	private void initGUI() {
		
	    GlyphTableModel glyphTableModel = new GlyphTableModel(_fontmodel, COLUMNCOUNT);
	    _fontmodel.addObserver(glyphTableModel);
	    
	    _glyphTable = new JTable(glyphTableModel);
		_glyphTable.setDefaultRenderer(Glyph.class, new GlyphTableCellRenderer());
		_glyphTable.setCellSelectionEnabled(true);
		_glyphTable.getSelectionModel().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		_glyphTable.getColumnModel().getSelectionModel().setSelectionMode(
		        							ListSelectionModel.SINGLE_SELECTION);
		_glyphTable.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				
				if (e.getClickCount() == 2) {	
				    try {
						JTable          source      = (JTable)e.getSource();
						GlyphTableModel tableModel  = (GlyphTableModel)source.getModel();
						Glyph           glyph       = (Glyph)source.getValueAt(source.getSelectedRow(),
						        											   source.getSelectedColumn());
						
						Glyph           clonedGlyph = (Glyph)glyph.clone();
				
						/*
						 * Een nieuw glyphComponentmodel maken.  Dit model wordt dan gebruikt 
						 * om de glyph te tekenen/wijzigen in het GlyphFrame.  
						 * Wijzigingen worden a/d copy doorgevoerd.
						 * 
						 * LET OP: enkel simpleGlyphs kunnen gewijzigd worden.
						 * CompositeGlyphs kun je enkel bekijken.
						 */
						
						AbstractGlyphFrameFactory componentFactory;
						
						if (clonedGlyph.getType().equals("composite")) {
						    componentFactory = new ReadOnlyGlyphFrameFactory(_fontmodel);
						}
						else {
						    componentFactory = new EditableGlyphFrameFactory(_fontmodel);
				    	}
				    	
						/*
						 * Het GlyphFrame opstellen of actief maken ( indien de glyph reeds geopend is)
						 */
						JInternalFrame[]   frames     = getDesktopPane().getAllFramesInLayer(2);
						AbstractGlyphFrame glyphFrame = null;
						
						// zoeken of het frame voor glyphX reeds gemaakt is
						for (int i=0; i<frames.length; i++) {
						    if (frames[i].getTitle().equals("glyph " + clonedGlyph.getID())) {
						        glyphFrame = (AbstractGlyphFrame)frames[i];
						    }
						}
						
						// frame maken of openen...
						if (glyphFrame == null) {
						    
						    //glyphFrame = new GlyphFrame("glyph " + clonedGlyph.getID(), componentFactory);
							glyphFrame = componentFactory.createGlyphFrame(clonedGlyph);
							getDesktopPane().add(glyphFrame, new Integer(2));
							glyphFrame.show(); 
						}
						else {
						    getDesktopPane().getDesktopManager().activateFrame(glyphFrame);
						}
				    }
					catch(CloneNotSupportedException ex) {
					    ex.printStackTrace();
					}
				}
			};
		});

		_panel = new JPanel();
		_scrollpane = new JScrollPane(_panel);
		
		_panel.add(_glyphTable);						
		getContentPane().add(_scrollpane);		
	}   
}

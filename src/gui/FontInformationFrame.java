/*
 * Created on 25-apr-2005
 */
package gui;


import javax.swing.*;
import javax.swing.border.BevelBorder;

import model.FontModel;

/**
 * @author Dimitri Gheysels
 */
public class FontInformationFrame extends JInternalFrame {

    private JPanel _panel;
        
    private JLabel _labelFontVersion;
	private JLabel _labelCopyright;
	private JLabel _labelFontFamily;
	private JLabel _labelFontName;
	private JLabel _labelTrademark;
	private JLabel _labelManufacturerName;
	private JLabel _labelDesignerName;
	private JLabel _labelTypefaceDescription;
	private JLabel _labelVendorURL;
	private JLabel _labelDesignerURL;
	private JLabel _labelLicenceDescription;
	private JLabel _labelLicenceURL;
	private JLabel _labelCreated;
	private JLabel _labelModified;
    
    private JTextField _textfieldFontVersion;
	private JTextField _textfieldCopyright;
	private JTextField _textfieldFontFamily;
	private JTextField _textfieldFontName;
	private JTextField _textfieldTrademark;
	private JTextField _textfieldManufacturerName;
	private JTextField _textfieldDesignerName;
	private JScrollPane  _typefacePane;
	private JTextArea _textfieldTypefaceDescription;
	private JTextField _textfieldVendorURL;
	private JTextField _textfieldDesignerURL;
	private JScrollPane  _licencePane;
	private JTextArea _textfieldLicenceDescription;
	private JTextField _textfieldLicenceURL;
	private JFormattedTextField _textfieldCreated;
	private JFormattedTextField _textfieldModified;
	
    private FontModel _fontmodel;
    
    public FontInformationFrame(String title, FontModel fontmodel) {
        super(title, true, true, true, true);
        _fontmodel = fontmodel; 
        setDefaultCloseOperation(JInternalFrame.HIDE_ON_CLOSE);   
        initGUI();  
    }
    
    private void initGUI() {
   
        getContentPane().setLayout(null);
        setBounds(25, 25, 770, 440);
        setResizable(false);
        setMaximizable(false);
        setIconifiable(false);
        
        _labelFontVersion = new JLabel("Fontversion: ");
        _labelFontVersion.setBounds(10, 10, 110, 20);
        _labelFontVersion.setHorizontalAlignment(SwingConstants.RIGHT);
        
        _labelFontName = new JLabel("Fontname: ");
        _labelFontName.setHorizontalAlignment(SwingConstants.RIGHT);
        _labelFontName.setBounds(10, 30, 110, 20);
        
        _labelFontFamily = new JLabel("Fontfamily: ");
        _labelFontFamily.setHorizontalAlignment(SwingConstants.RIGHT);
        _labelFontFamily.setBounds(10, 50, 110, 20);
        
        _labelTrademark = new JLabel("Trademark: ");
        _labelTrademark.setHorizontalAlignment(SwingConstants.RIGHT);
        _labelTrademark.setBounds(10, 70, 110, 20);
        
        _labelManufacturerName = new JLabel("Manufacturer: ");
        _labelManufacturerName.setHorizontalAlignment(SwingConstants.RIGHT);
        _labelManufacturerName.setBounds(10, 90, 110, 20);
        
        _labelDesignerName = new JLabel("Designer: "); 
        _labelDesignerName.setHorizontalAlignment(SwingConstants.RIGHT);
        _labelDesignerName.setBounds(10, 110, 110, 20);
        
        _labelDesignerURL = new JLabel("Designer URL: ");
        _labelDesignerURL.setHorizontalAlignment(SwingConstants.RIGHT);
        _labelDesignerURL.setBounds(10, 130, 110, 20);
        
        _labelVendorURL = new JLabel("Vendor URL: ");
        _labelVendorURL.setHorizontalAlignment(SwingConstants.RIGHT);
        _labelVendorURL.setBounds(10, 150, 110, 20);
        
        _labelTypefaceDescription = new JLabel("Typeface: ");
        _labelTypefaceDescription.setHorizontalAlignment(SwingConstants.RIGHT);
        _labelTypefaceDescription.setBounds(10, 170, 110, 20);
        
        _labelLicenceDescription = new JLabel("Licence: ");
        _labelLicenceDescription.setHorizontalAlignment(SwingConstants.RIGHT);
        _labelLicenceDescription.setBounds(10, 230, 110, 20);
        
        _labelLicenceURL = new JLabel("Licence URL: ");
        _labelLicenceURL.setHorizontalAlignment(SwingConstants.RIGHT);
        _labelLicenceURL.setBounds(10, 290, 110, 20);
        
        _labelCreated = new JLabel("Created date: ");
        _labelCreated.setHorizontalAlignment(SwingConstants.RIGHT);
        _labelCreated.setBounds(10, 310, 110, 20);
        
        _labelModified = new JLabel("Modified date: ");
        _labelModified.setHorizontalAlignment(SwingConstants.RIGHT);
        _labelModified.setBounds(10, 330, 110, 20);
        
        _labelCopyright = new JLabel("Copyright: ");
        _labelCopyright.setHorizontalAlignment(SwingConstants.RIGHT);
        _labelCopyright.setBounds(10, 350, 110, 20);
        
        _textfieldFontVersion = new JTextField(_fontmodel.getFontInformation().getFontVersion());
        _textfieldFontVersion.setBounds(130, 10, 598, 20);
        
        _textfieldFontName = new JTextField(_fontmodel.getFontInformation().getFontName());
        _textfieldFontName.setBounds(130, 30, 598, 20);
        
        _textfieldFontFamily = new JTextField(_fontmodel.getFontInformation().getFontFamily());
        _textfieldFontFamily.setBounds(130, 50, 598, 20);
        
        _textfieldTrademark = new JTextField(_fontmodel.getFontInformation().getTrademark());
        _textfieldTrademark.setBounds(130, 70, 598, 20);
        
        _textfieldManufacturerName = new JTextField(_fontmodel.getFontInformation().getManufacturerName());
        _textfieldManufacturerName.setBounds(130, 90, 598, 20);
        
        _textfieldDesignerName = new JTextField(_fontmodel.getFontInformation().getDesignerName());
        _textfieldDesignerName.setBounds(130, 110, 598, 20);
        
        _textfieldDesignerURL = new JTextField(_fontmodel.getFontInformation().getDesignerURL());
        _textfieldDesignerURL.setBounds(130, 130, 598, 20);
        
        _textfieldVendorURL = new JTextField(_fontmodel.getFontInformation().getVendorURL());
        _textfieldVendorURL.setBounds(130, 150, 598, 20);
        
        _textfieldTypefaceDescription = new JTextArea(_fontmodel.getFontInformation().getTypefaceDescription());
        _textfieldTypefaceDescription.setBorder(BorderFactory.createEtchedBorder(BevelBorder.LOWERED));
        _textfieldTypefaceDescription.setLineWrap(true);
        _textfieldTypefaceDescription.setWrapStyleWord(true);
        _textfieldTypefaceDescription.setBounds(130, 170, 598, 60);
        
        _textfieldLicenceDescription = new JTextArea(_fontmodel.getFontInformation().getLicenceDescription());
        _textfieldLicenceDescription.setBorder(BorderFactory.createEtchedBorder(BevelBorder.LOWERED));
        _textfieldLicenceDescription.setLineWrap(true);
        _textfieldLicenceDescription.setWrapStyleWord(true);
        _textfieldLicenceDescription.setBounds(130, 230, 598, 60);
        
        _textfieldLicenceURL = new JTextField(_fontmodel.getFontInformation().getLicenceURL());
        _textfieldLicenceURL.setBounds(130, 290, 598, 20);
    
        _textfieldCreated = new JFormattedTextField(_fontmodel.getFontInformation().getCreated());
        _textfieldCreated.setBounds(130, 310, 598, 20);
        
        _textfieldModified = new JFormattedTextField(_fontmodel.getFontInformation().getModified());
        _textfieldModified.setBounds(130, 330, 598, 20);
        
        _textfieldCopyright = new JTextField(_fontmodel.getFontInformation().getCopyright());
        _textfieldCopyright.setBounds(130, 350, 598, 20);
        
        getContentPane().add(_labelFontVersion);
        getContentPane().add(_labelFontName);
        getContentPane().add(_labelFontFamily);
        getContentPane().add(_labelTrademark);  
        getContentPane().add(_labelVendorURL);
        getContentPane().add(_labelManufacturerName);        
        getContentPane().add(_labelDesignerName);
        getContentPane().add(_labelDesignerURL);
        getContentPane().add(_labelTypefaceDescription);
        getContentPane().add(_labelLicenceDescription);
        getContentPane().add(_labelLicenceURL);
        getContentPane().add(_labelCreated);
        getContentPane().add(_labelModified);
        getContentPane().add(_labelCopyright);
        
        getContentPane().add(_textfieldFontVersion);
        getContentPane().add(_textfieldFontName);
        getContentPane().add(_textfieldFontFamily);
        getContentPane().add(_textfieldTrademark);      
        getContentPane().add(_textfieldManufacturerName);
        getContentPane().add(_textfieldDesignerName);
        getContentPane().add(_textfieldDesignerURL);
        getContentPane().add(_textfieldVendorURL);
        getContentPane().add(_textfieldTypefaceDescription);
        getContentPane().add(_textfieldLicenceDescription);
        getContentPane().add(_textfieldLicenceURL);
        getContentPane().add(_textfieldCreated);
        getContentPane().add(_textfieldModified);
        getContentPane().add(_textfieldCopyright);
    }
}

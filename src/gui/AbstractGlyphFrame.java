/*
 * Created on 2-mei-2005
 */
package gui;

import java.awt.*;
import java.awt.event.ActionEvent;
import javax.swing.*;

import gui.components.AbstractGlyphComponent;

/**
 * @author Dimitri Gheysels
 */
public abstract class AbstractGlyphFrame extends JInternalFrame{
    protected FlowLayout _buttonPanelLayout;
    protected JPanel     _buttonPanel;
	protected JButton    _buttonClose; 
	protected Action     _closeAction;
	
	protected AbstractGlyphComponent _glyphComponent;
	//protected FontModel _fontmodel; 
	
	protected AbstractGlyphFrame(String title, /*FontModel fontmodel*/AbstractGlyphComponent c) {
		super(title, true, true, true, true);
		_glyphComponent = c;
		//_fontmodel = fontmodel;
		setClosable(false);
		//initGUI();
	}
	
	protected void initGUI() {
	    _buttonPanelLayout = new FlowLayout(FlowLayout.RIGHT);
		_buttonPanel       = new JPanel(_buttonPanelLayout);
		_closeAction       = new CloseAction();
		_buttonClose       = new JButton(_closeAction);

		_buttonPanel.add(_buttonClose);
		
		getContentPane().add(_glyphComponent, BorderLayout.CENTER);
		getContentPane().add(_buttonPanel, BorderLayout.SOUTH);
				
		setBounds(10, 10, _glyphComponent.getWidth(), _glyphComponent.getHeight());	 
	}
			
	protected class CloseAction extends AbstractAction {
	    
	    public CloseAction() {
	        super("Close");
	    }
	   	   
	    public void actionPerformed(ActionEvent e) {
	        dispose();
	    }
	}
}

/*
 * Created on 2-mei-2005
 */
package gui.glyphFrameFactory;

import gui.AbstractGlyphFrame;
import gui.components.AbstractGlyphComponent;
import gui.components.AbstractGlyphComponentModel;
import model.FontModel;
import model.Glyph;

/**
 * @author Dimitri Gheysels
 */
public abstract class AbstractGlyphFrameFactory {

    protected FontModel _fontmodel;
    
    protected AbstractGlyphFrameFactory(FontModel m) {
        _fontmodel = m;
    }
    
    protected  abstract AbstractGlyphComponent createGlyphComponent(AbstractGlyphComponentModel m);
    public  abstract AbstractGlyphFrame     createGlyphFrame(Glyph g);
}

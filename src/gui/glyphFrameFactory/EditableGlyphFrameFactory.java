/*
 * Created on 2-mei-2005
 */
package gui.glyphFrameFactory;

import gui.*;
import gui.components.*;
import model.FontModel;
import model.Glyph;

/**
 * @author Dimitri Gheysels
 */
public class EditableGlyphFrameFactory extends AbstractGlyphFrameFactory {

    public EditableGlyphFrameFactory(FontModel m) {
        super(m);
    }
    
    protected AbstractGlyphComponent createGlyphComponent(AbstractGlyphComponentModel m) {
        return new EditableGlyphComponent((EditableGlyphComponentModel)m);
    }

    public AbstractGlyphFrame createGlyphFrame(Glyph g) {
        AbstractGlyphComponentModel m = new EditableGlyphComponentModel(_fontmodel);
        m.setGlyph(g);
        
        return new EditableGlyphFrame("Glyph " + g.getID(), 
                createGlyphComponent(m));
    }
}

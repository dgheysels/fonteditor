/*
 * Created on 2-mei-2005
 */
package gui.glyphFrameFactory;

import gui.*;
import gui.components.*;
import model.*;

/**
 * @author Dimitri Gheysels
 */
public class ReadOnlyGlyphFrameFactory extends AbstractGlyphFrameFactory {

    public ReadOnlyGlyphFrameFactory(FontModel m) {
        super(m);
    }
    
    protected AbstractGlyphComponent createGlyphComponent(AbstractGlyphComponentModel m) {
        return new ReadOnlyGlyphComponent((ReadOnlyGlyphComponentModel)m);
    }

    public AbstractGlyphFrame createGlyphFrame(Glyph g) {
        AbstractGlyphComponentModel m = new ReadOnlyGlyphComponentModel(_fontmodel);
        m.setGlyph(g);
        
        return new ReadOnlyGlyphFrame("Glyph " + g.getID(), 
                createGlyphComponent(m));
    }
}

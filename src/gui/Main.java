/*
 * Created on 14-apr-2005
 *
 * TODO To change the template for this generated file go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
package gui;

import java.awt.event.ActionEvent;
import java.io.IOException;
import javax.swing.*;

import model.*;


/**
 * @author Dimitri Gheysels
 *
 * TODO To change the template for this generated type comment go to
 * Window - Preferences - Java - Code Style - Code Templates
 */
public class Main extends JFrame {
	
	// GUI components
	private JDesktopPane _desktopPane;
	
	private JMenuBar     _menubar;
	private JMenu        _menuFile;
	private JMenu        _menuEdit;
	private JMenu        _menuView;
	
	private JMenuItem    _menuFileOpen;
	private JMenuItem    _menuFileSave;
	private JMenuItem    _menuFileSaveAs;
	private JMenuItem    _menuFileClose;
	private JSeparator   _menuFileSeparator;
	private JMenuItem    _menuFileExit;
	private JMenuItem    _menuEditSettings;
	private JMenuItem    _menuViewFontinformation;
	private JMenuItem    _menuViewGlyphtable;
	
	private Action       _openFontAction;
	private Action       _saveFontAction;
	private Action       _saveAsFontAction;
	private Action       _closeFontAction;
	private Action       _exitFontAction;
	private Action       _editSettingsAction;
	private Action       _viewFontinformationAction;
	private Action       _viewGlyphtableAction;
	
	private FontModel    _fontmodel;
	
	public Main(String title) {
		super(title);	
		_fontmodel = new FontModel();	
		initGUI();			
		pack();	
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setExtendedState(JFrame.MAXIMIZED_BOTH);	
	}
	
	private void initGUI() {
		
		_desktopPane = new JDesktopPane();
		getContentPane().add(_desktopPane);
		
		_openFontAction = new OpenFontAction();
		_saveFontAction = new SaveFontAction();
		_saveAsFontAction = new SaveAsFontAction();
		_closeFontAction = new CloseFontAction();
		_exitFontAction = new ExitFontAction();
		_editSettingsAction = new EditSettingsAction();
		_viewFontinformationAction = new ViewFontinformationAction();
		_viewGlyphtableAction = new ViewGlyphtableAction();
		
		_saveFontAction.setEnabled(false);
		_saveAsFontAction.setEnabled(false);
		_closeFontAction.setEnabled(false);
		_viewFontinformationAction.setEnabled(false);
		_viewGlyphtableAction.setEnabled(false);
				
		_menubar = new JMenuBar();
		_menuFile = new JMenu("File");
		_menuEdit = new JMenu("Edit");
		_menuView = new JMenu("View");
		
		_menuFileOpen = new JMenuItem(_openFontAction);
		_menuFileSave = new JMenuItem(_saveFontAction);
		_menuFileSaveAs = new JMenuItem(_saveAsFontAction);
		_menuFileClose = new JMenuItem(_closeFontAction);
		_menuFileSeparator = new JSeparator();
		_menuFileExit = new JMenuItem(_exitFontAction);
		_menuEditSettings = new JMenuItem(_editSettingsAction);	
		_menuViewFontinformation = new JMenuItem(_viewFontinformationAction);
		_menuViewGlyphtable = new JMenuItem(_viewGlyphtableAction);
		
		_menuFile.add(_menuFileOpen);
		_menuFile.add(_menuFileSave);
		_menuFile.add(_menuFileSaveAs);
		_menuFile.add(_menuFileClose);
		_menuFile.add(_menuFileSeparator);
		_menuFile.add(_menuFileExit);
		_menuEdit.add(_menuEditSettings);
		_menuView.add(_menuViewFontinformation);
		_menuView.add(_menuViewGlyphtable);
		
		_menubar.add(_menuFile);
		_menubar.add(_menuEdit);
		_menubar.add(_menuView);
		
		setJMenuBar(_menubar);		
	}
	
	private class OpenFontAction extends AbstractAction {

		public OpenFontAction() {
			super("Open...");
		}
		
		public void actionPerformed(ActionEvent e) {
			
		    try {
				JFileChooser openfile = new JFileChooser();
				openfile.setFileFilter(new FontFileFilter());
				
				if (_fontmodel.isFontLoaded()) {
				    int answer = JOptionPane.showConfirmDialog(
                    		Main.this, 
                   			"A font is already loaded.  Would you like to save it?",
                   			"Save font",
                   			JOptionPane.YES_NO_CANCEL_OPTION,
                   			JOptionPane.QUESTION_MESSAGE);
				    
				    switch(answer) {
				    	case JOptionPane.YES_OPTION : _saveAsFontAction.actionPerformed(e);
				    	case JOptionPane.NO_OPTION  : _closeFontAction.actionPerformed(e); break;
				    }
				}
				
				if (openfile.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
				    
				    _fontmodel = new FontModel();
				    _fontmodel.load(openfile.getSelectedFile().getAbsolutePath());
				    
				    GlyphTableFrame glyphTableFrame = new GlyphTableFrame("glyphtable", _fontmodel);
				    FontInformationFrame fontinfoFrame = new FontInformationFrame("fontinformation", _fontmodel);
				    
				    _desktopPane.add(glyphTableFrame, new Integer(1));
				    _desktopPane.add(fontinfoFrame, new Integer(1));
				    _desktopPane.getDesktopManager().maximizeFrame(glyphTableFrame);						
				    
				    glyphTableFrame.show();	
				    fontinfoFrame.show();
				    
				    _closeFontAction.setEnabled(true);	
				    _saveFontAction.setEnabled(true);
				    _saveAsFontAction.setEnabled(true);
				    _viewFontinformationAction.setEnabled(true);
				    _viewGlyphtableAction.setEnabled(true);
				}			
			}
			catch(IOException ex) {
				JOptionPane.showMessageDialog(null, ex.getMessage(), "error", JOptionPane.ERROR_MESSAGE);
			}	
		}
	}
	
	private class SaveFontAction extends AbstractAction {
		
		public SaveFontAction() {
			super("Save");
		}
		
		public void actionPerformed(ActionEvent e) {
			
		    try {
		        _fontmodel.save();
		    }
		    catch(IOException ex) {
		        JOptionPane.showInternalMessageDialog(
		                		Main.this, 
	                   			ex.getMessage(),
	                   			"Error",
	                   			JOptionPane.ERROR_MESSAGE);
		    }
		}
	}
	
	private class SaveAsFontAction extends AbstractAction {
		
		public SaveAsFontAction() {
			super("Save As...");
		}
		
		public void actionPerformed(ActionEvent e) {
		    
		    try {
		        JFileChooser savefile = new JFileChooser();
		        		        
		        if (savefile.showSaveDialog(null) == JFileChooser.APPROVE_OPTION) {
		            _fontmodel.save(savefile.getSelectedFile().getAbsolutePath());		            			
		        }
		    }
		    catch(IOException ex) {
		        JOptionPane.showMessageDialog(null, ex.getMessage(), "error", JOptionPane.ERROR_MESSAGE);
		    }
		}
	}
	
	private class CloseFontAction extends AbstractAction {
		
		public CloseFontAction() {
			super("Close");
		}
		
		public void actionPerformed(ActionEvent e) {
			
			try {
				JInternalFrame[] frames = _desktopPane.getAllFrames();
				
				for (int i=0; i<frames.length; i++) {
					frames[i].dispose();
				}
									
				_fontmodel.close();
				
				_closeFontAction.setEnabled(false);	
				_saveFontAction.setEnabled(false);
				_saveAsFontAction.setEnabled(false);
				_viewFontinformationAction.setEnabled(false);
				_viewGlyphtableAction.setEnabled(false);
			}
			catch(IOException ex) {
				JOptionPane.showMessageDialog(null, ex.getMessage(), "error", JOptionPane.ERROR_MESSAGE);
			}
		}
	}

	private class ExitFontAction extends AbstractAction {
		
		public ExitFontAction() {
			super("Exit");
		}
		
		public void actionPerformed(ActionEvent e) {	
			dispose();
		}
	}

	// NOT YET IMPLEMENTED
	private class EditSettingsAction extends AbstractAction {
		
		public EditSettingsAction() {
			super("Settings...");
		}
		
		public void actionPerformed(ActionEvent e) {
			
		}
	}

	private class ViewFontinformationAction extends AbstractAction {
	    
	    public ViewFontinformationAction() {
	        super("Fontinformation");
	    }
	    
	    public void actionPerformed(ActionEvent e) {
	        
	       JInternalFrame[] frames = _desktopPane.getAllFramesInLayer(1);
	        
	        for (int i=0; i<frames.length; i++) {
			    if (frames[i].getTitle().equals("fontinformation")) {
			        frames[i].show();
			        break;
			    }
	        }
	    }
	}
	
	private class ViewGlyphtableAction extends AbstractAction {
	    
	    public ViewGlyphtableAction() {
	        super("Glyphtable");
	    }
	    
	    public void actionPerformed(ActionEvent e) {
	        
	        JInternalFrame[] frames = _desktopPane.getAllFramesInLayer(1);
	        
	        for (int i=0; i<frames.length; i++) {
			    if (frames[i].getTitle().equals("glyphtable")) {
			        frames[i].show();
			        break;
			    }
	        }    
	    }
	}

	/*
	 * Entry-point v/d applicatie
	 */
	public static void main(String[] args) {
	    try {
	        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
	        UIManager.put("ReadOnlyGlyphComponentUI", "gui.components.ReadOnlyGlyphComponentUI");
	        UIManager.put("EditableGlyphComponentUI", "gui.components.EditableGlyphComponentUI");
	        
	        new Main("fonteditor").setVisible(true);
	    }
	    catch(ClassNotFoundException ex) { 
	        JOptionPane.showMessageDialog(null, "ERROR", ex.getMessage(), JOptionPane.ERROR_MESSAGE);
	    }
	    catch(InstantiationException ex) { 
	        JOptionPane.showMessageDialog(null, "ERROR", ex.getMessage(), JOptionPane.ERROR_MESSAGE);
	    }
	    catch(IllegalAccessException ex) {
	        JOptionPane.showMessageDialog(null, "ERROR", ex.getMessage(), JOptionPane.ERROR_MESSAGE);
	    }
	    catch(UnsupportedLookAndFeelException ex) { 
	        JOptionPane.showMessageDialog(null, "ERROR", ex.getMessage(), JOptionPane.ERROR_MESSAGE);
	    }	    
	}
}



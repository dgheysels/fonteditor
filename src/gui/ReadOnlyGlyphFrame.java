/*
 * Created on 2-mei-2005
 */
package gui;

import gui.components.AbstractGlyphComponent;

public class ReadOnlyGlyphFrame extends AbstractGlyphFrame {

    public ReadOnlyGlyphFrame(String title, /*FontModel fontmodel*/AbstractGlyphComponent c) {	
		super(title, /*fontmodel*/c);
		//_glyphComponent = new ReadOnlyGlyphComponent(new ReadOnlyGlyphComponentModel(fontmodel));
		initGUI();			
	}
    
    protected void initGUI() {
        super.initGUI();   
    }     
}

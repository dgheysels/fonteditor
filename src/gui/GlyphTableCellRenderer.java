/*
 * Created on 6-apr-2005
 */
package gui;

import java.awt.*;
import javax.swing.table.*;
import javax.swing.*;

import gui.components.*;
import model.Glyph;

/**
 * This renders the glyphs onto the grid of the JTable component
 * 
 * @author Dimitri Gheysels
 */
public class GlyphTableCellRenderer extends ReadOnlyGlyphComponent implements TableCellRenderer, IGUIConstants {

	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {
		
	    GlyphTableModel m = (GlyphTableModel)table.getModel();
			    
		ReadOnlyGlyphComponentModel glyphComponentModel =
		    new ReadOnlyGlyphComponentModel(m.getFontModel());	
	    
		glyphComponentModel.setGlyph((Glyph)value);
		setModel(glyphComponentModel);	
		setPointSize(SMALLGLYPHSIZE);
		
		if (table.getRowHeight(row) != getHeight()) {
			table.setRowHeight(getHeight());
		}
		
		if (table.getColumnModel().getColumn(column).getPreferredWidth() != getWidth()) {
			table.getColumnModel().getColumn(column).setPreferredWidth(getWidth());
		}
						
		if (isSelected) {
			setBorder(BorderFactory.createLineBorder(Color.BLUE));
		}
		else {
			setBorder(null);
		}
		
		return this;
	}	
}

/*
 * Created on 28-apr-2005
 */
package gui.state;

import java.awt.event.MouseEvent;

import model.FontInformation;
import extra.Convert;
import gui.components.EditableGlyphComponent;

/**
 * @author Dimitri Gheysels
 */
public class SelectPointState extends State {

    protected void handleMouseClicked(MouseEvent e, EditableGlyphComponent c) {
       
        FontInformation fi = c.getFontInformation();
        
        // links klikken = punt selecteren
	    if (e.getButton() == MouseEvent.BUTTON1) {
	    	    	
	        // pixels -> funits
			int x = Convert.pixelToFUnit(e.getX(), c.getPPEM(), fi.getUnitsPerEM());
			int y = -Convert.pixelToFUnit(e.getY(), c.getPPEM(), fi.getUnitsPerEM());
			
			c.setSelectedPoint(c.getGlyph().getPoint(x, y, PRECISION));		
			c.repaint();
	    }
	    // rechts klikken = selectie opheffen
	    else {
	        c.setSelectedPoint(null);
	        c.repaint();
	    }  
    }

    protected void handleMouseDragged(MouseEvent e, EditableGlyphComponent c) {
        
        FontInformation fi = c.getFontInformation();
		
		if (c.getSelectedPoint() != null) {		
			// pixels -> funits
			c.getSelectedPoint().x = Convert.pixelToFUnit(e.getX(), c.getPPEM(), fi.getUnitsPerEM());
			c.getSelectedPoint().y = -Convert.pixelToFUnit(e.getY(), c.getPPEM(), fi.getUnitsPerEM());
			
			c.setModified(true);
			c.repaint();
	    }
    }
}

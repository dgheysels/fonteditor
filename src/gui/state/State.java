/*
 * Created on 28-apr-2005
 */
package gui.state;

import gui.IGUIConstants;
import gui.components.EditableGlyphComponent;
import java.awt.event.MouseEvent;

import model.FontInformation;
import extra.Convert;

/**
 * @author Dimitri Gheysels
 */
public abstract class State implements IGUIConstants{

    protected abstract void handleMouseClicked(MouseEvent e, EditableGlyphComponent c);
    protected abstract void handleMouseDragged(MouseEvent e, EditableGlyphComponent c);
    
    public final void mouseClicked(MouseEvent e) {
        
        EditableGlyphComponent c = (EditableGlyphComponent)e.getComponent();

		e = translatePoint(e, c);
        handleMouseClicked(e, c);
    }
    
    public final void mouseDragged(MouseEvent e) {
        
        EditableGlyphComponent c = (EditableGlyphComponent)e.getComponent();
	
		e = translatePoint(e, c);		        
		handleMouseDragged(e, c);
    }  
    
    private MouseEvent translatePoint(MouseEvent e, EditableGlyphComponent c) {
        FontInformation fi = c.getFontInformation();
        
        e.translatePoint(Convert.funitsToPixel(fi.getXMin(), c.getPPEM(), fi.getUnitsPerEM()),
		        		 -(c.getHeight() - Math.abs(Convert.funitsToPixel(fi.getYMin(), c.getPPEM(), fi.getUnitsPerEM()))));
        
        return e;
    }
}

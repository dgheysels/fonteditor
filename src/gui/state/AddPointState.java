/*
 * Created on 28-apr-2005
 */
package gui.state;

import java.awt.event.MouseEvent;

import extra.Convert;
import gui.components.EditableGlyphComponent;
import model.*;

/**
 * @author Dimitri Gheysels
 */
public class AddPointState extends State {

    protected void handleMouseClicked(MouseEvent e, EditableGlyphComponent c) {
    	
        FontInformation fi = c.getFontInformation();
		boolean onCurve;
		
		// Als de CTRL-toets is ingeduwt tijdens het klikken, dan plaatsen we 
		// een offcurve point
		if ((e.getModifiersEx() & MouseEvent.CTRL_DOWN_MASK) != 0) {
		    onCurve = false;
		}
		else {
		    onCurve = true;
		}
        
		// pixels -> funits
		int x = Convert.pixelToFUnit(e.getX(), c.getPPEM(), fi.getUnitsPerEM());
		int y = -Convert.pixelToFUnit(e.getY(), c.getPPEM(), fi.getUnitsPerEM());
		
		Point beforePoint = c.getSelectedPoint();
		Point newPoint   = new Point(beforePoint.getID()+1, x, y, onCurve);
				
		c.getGlyph().addPoint(newPoint);
				
		c.setSelectedPoint(newPoint);
		c.setModified(true);
		
		c.repaint();
    }

    protected void handleMouseDragged(MouseEvent e, EditableGlyphComponent c) {}
}

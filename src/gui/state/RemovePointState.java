/*
 * Created on 28-apr-2005
 */
package gui.state;

import java.awt.event.MouseEvent;

import extra.Convert;
import gui.components.EditableGlyphComponent;
import model.*;

/**
 * @author Dimitri Gheysels
 */
public class RemovePointState extends State {
 
    protected void handleMouseClicked(MouseEvent e, EditableGlyphComponent c) {
        
        FontInformation fi = c.getFontInformation();
		        
		// pixels -> funits
		int x = Convert.pixelToFUnit(e.getX(), c.getPPEM(), fi.getUnitsPerEM());
		int y = -Convert.pixelToFUnit(e.getY(), c.getPPEM(), fi.getUnitsPerEM());
		
		Point p = c.getGlyph().getPoint(x, y, PRECISION);
		
		if (p != null) {
		    c.getGlyph().removePoint(p);
		    c.setModified(true);
		    c.repaint();
		}
    }

    protected void handleMouseDragged(MouseEvent e, EditableGlyphComponent c ) {}
}

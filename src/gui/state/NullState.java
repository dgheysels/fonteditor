/*
 * Created on 4-mei-2005
 */
package gui.state;

import gui.components.EditableGlyphComponent;

import java.awt.event.MouseEvent;

/**
 * @author Dimitri Gheysels
 */
public class NullState extends State {
 
    protected void handleMouseClicked(MouseEvent e, EditableGlyphComponent c) {}
    protected void handleMouseDragged(MouseEvent e, EditableGlyphComponent c) {}

}

/*
 * Created on 2-mei-2005
 */
package gui;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.beans.*;
import javax.swing.*;

import gui.components.*;
import gui.state.*;

/**
 * @author Dimitri Gheysels
 */
public class EditableGlyphFrame extends AbstractGlyphFrame implements PropertyChangeListener {
	private JButton    _buttonSave;
	
	private JToolBar      _toolbar;
	private ButtonGroup   _stateButtons;
	private JToggleButton _buttonSelectPoint;
	private JToggleButton _buttonAddPoint;
	private JToggleButton _buttonRemovePoint;
	
	// actions worden aan buttons gekoppeld
	private Action     _selectPointStateAction;
	private Action     _addPointStateAction;
	private Action     _removePointStateAction;
	private Action     _saveAction;
		 
	public EditableGlyphFrame(String title, /*FontModel fontmodel*/AbstractGlyphComponent c) {
		super(title, /*fontmodel*/c);
		//EditableGlyphComponentModel m = new EditableGlyphComponentModel(fontmodel);
		//m.setgl
		//_glyphComponent = new EditableGlyphComponent(m);
		_glyphComponent.addPropertyChangeListener(this);					
		initGUI();
	}
	
	protected void initGUI() {
	    super.initGUI();
	    		
		_selectPointStateAction = new SetStateAction(new SelectPointState(), "select point");
		_addPointStateAction    = new SetStateAction(new AddPointState(), "add point");
		_removePointStateAction = new SetStateAction(new RemovePointState(), "remove point");
		_saveAction             = new SaveAction();
		_closeAction            = new CloseAction();
			
		_buttonSelectPoint = new JToggleButton(_selectPointStateAction);
		_buttonAddPoint    = new JToggleButton(_addPointStateAction);
		_buttonRemovePoint = new JToggleButton(_removePointStateAction);
		_buttonClose.setAction(_closeAction);
		_buttonSave        = new JButton(_saveAction);
				
		_toolbar           = new JToolBar();
		_stateButtons      = new ButtonGroup();
		
		_stateButtons.add(_buttonSelectPoint);
		_stateButtons.add(_buttonAddPoint);
		_stateButtons.add(_buttonRemovePoint);
		
		_toolbar.setFloatable(false);
		_toolbar.add(_buttonSelectPoint);
		_toolbar.add(_buttonAddPoint);
		_toolbar.add(_buttonRemovePoint);
				
		_buttonPanel.add(_buttonSave, 0);
		
		getContentPane().add(_toolbar, BorderLayout.NORTH);
	}
	
	private class SetStateAction extends AbstractAction {
	    
	    private State _state;
	    
	    public SetStateAction(State s, String name) {
	        super(name);
	        _state = s;
	    }
	    
	    public void actionPerformed(ActionEvent e) {
	       ((EditableGlyphComponent)_glyphComponent).setState(_state);
	    }
	}
	
	private class SaveAction extends AbstractAction {
	    
	    public SaveAction() {
	        super("Save");
	        setEnabled(false);
	    }
	    
	    public void actionPerformed(ActionEvent e) {
	        ((EditableGlyphComponent)_glyphComponent).save();
	    }
	}
	
	protected class CloseAction extends AbstractAction {
	    
	    public CloseAction() {
	        super("Close");
	    }
	    
	    public void actionPerformed(ActionEvent e) {
	        if (((EditableGlyphComponent)_glyphComponent).isModified()) {
	            int answer = JOptionPane.showInternalConfirmDialog(
	                    		EditableGlyphFrame.this, 
	                   			"This glyph is modified.  Would you like to save it?",
	                   			"Save glyph",
	                   			JOptionPane.YES_NO_CANCEL_OPTION,
	                   			JOptionPane.QUESTION_MESSAGE);
	            
	            switch(answer) {
	            	case JOptionPane.NO_OPTION : dispose(); break;
	            	case JOptionPane.YES_OPTION :
	            	    ((EditableGlyphComponent)_glyphComponent).save();
	            		dispose();
	            		break;
	            }
	      	}
		    else {
		        dispose();
		    } 
	    }
	}
	    
    /*
     * Wanneer bepaalde eigenschappen van het glyphcomponent gewijzigd zijn, wordt dit frame
     * ervan op de hoogte gehouden.
     */
    public void propertyChange(PropertyChangeEvent e) {
        
        if (e.getPropertyName().equals("modified")) {
            _saveAction.setEnabled(Boolean.parseBoolean(e.getNewValue().toString()));
        }
    }
}

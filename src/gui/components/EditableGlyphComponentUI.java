/*
 * Created on 2-mei-2005
 */
package gui.components;

import java.awt.*;
import java.awt.event.MouseEvent;
import javax.swing.JComponent;
import javax.swing.event.MouseInputListener;
import javax.swing.plaf.ComponentUI;

/**
 * @author Dimitri Gheysels
 */
public class EditableGlyphComponentUI extends AbstractGlyphComponentUI implements MouseInputListener {

    public static ComponentUI createUI(JComponent c) {
		return new EditableGlyphComponentUI();
	}
    
    public void installUI(JComponent c) {
		EditableGlyphComponent glyphComponent = (EditableGlyphComponent)c;
		glyphComponent.addMouseListener(this);
		glyphComponent.addMouseMotionListener(this);
	}
	
	public void uninstallUI(JComponent c) {
		EditableGlyphComponent glyphComponent = (EditableGlyphComponent)c;
		glyphComponent.removeMouseListener(this);
		glyphComponent.removeMouseMotionListener(this);
	}
	
	public void paint(Graphics g, JComponent c) {
	    setDrawPoints(true);
	    setSelectedPoint(((EditableGlyphComponent)c).getSelectedPoint());
	    super.paint((Graphics2D)g, (AbstractGlyphComponent)c);
    }
	
	public void mouseDragged(MouseEvent e) {
		EditableGlyphComponent c = (EditableGlyphComponent)e.getComponent();		
		c.getState().mouseDragged(e);		
	}
	
	public void mouseClicked(MouseEvent e) {
	    EditableGlyphComponent c = (EditableGlyphComponent)e.getComponent();	    
	    c.getState().mouseClicked(e);
	}		
	
	public void mouseReleased(MouseEvent e) {}
	public void mousePressed(MouseEvent e) {}
	public void mouseMoved(MouseEvent e) {}
	public void mouseEntered(MouseEvent e) {}
	public void mouseExited(MouseEvent e) {}
}

/*
 * Created on 2-mei-2005
 */
package gui.components;

import javax.swing.UIManager;

import model.Point;
import gui.IGUIConstants;
import gui.state.*;

/**
 * @author Dimitri Gheysels
 */
public class EditableGlyphComponent extends AbstractGlyphComponent implements IGUIConstants {

    private EditableGlyphComponentModel _model;
    
    private State _componentState;
    private Point _selectedPoint;
    
    public EditableGlyphComponent(EditableGlyphComponentModel model) {
        _model = model;
        setPointSize(LARGEGLYPHSIZE);
        setState(new NullState());
	}
    
    public void save() {
        _model.save();
        setModified(false);
    }
    
    /*
     * GETTERS / SETTERS
     */ 
    public AbstractGlyphComponentModel getModel() { return _model; }
    public State getState() { return _componentState; }
    public Point getSelectedPoint() { return _selectedPoint; }
    public boolean isModified() { return _model.isModified(); }
    
    public void setModel(EditableGlyphComponentModel model) { _model = model; }
    public void setSelectedPoint(Point p) { _selectedPoint = p; }
    public void setState(State s) { _componentState = s; }
    public void setModified(boolean value) {
        boolean old = isModified();
	    
	    if (old != value) {
	        _model.setModified(value);
	        firePropertyChange("modified", old, value);
	    }
	}
    
    /*
	 * Methods voor ComponentUI-delegate
	 * Deze worden gebruikt door het Swing-Framework
	 */
	public void setUI(AbstractGlyphComponentUI componentUI) {
		super.setUI(componentUI);
	}
	
	public void updateUI() {
		setUI((AbstractGlyphComponentUI)UIManager.getUI(this));
		invalidate();
	}
	
	public String getUIClassID() {
		return "EditableGlyphComponentUI";
	}
}

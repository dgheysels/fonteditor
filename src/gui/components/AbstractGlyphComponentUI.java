/*
 * Created on 2-mei-2005
 */
package gui.components;

import java.awt.*;
import java.awt.geom.*;
import java.util.*;
import javax.swing.plaf.ComponentUI;

import extra.*;
import model.*;
import gui.*;

/**
 * @author Dimitri Gheysels
 */
public abstract class AbstractGlyphComponentUI extends ComponentUI implements IGUIConstants{

    private boolean 	_drawPoints = false;
    private model.Point _selectedPoint;
    
    public AbstractGlyphComponentUI() {}

    public void setDrawPoints(boolean b) {
        _drawPoints = b;
    }
    
    public void setSelectedPoint(model.Point p) {
        _selectedPoint = p;
    }
    
    public void paint(Graphics2D g2d, AbstractGlyphComponent component/*, 
            		  boolean drawPoints, model.Point selectedPoint*/) {
       
        AbstractGlyphComponentModel model    = (AbstractGlyphComponentModel)component.getModel();
		Glyph       			    glyph    = model.getGlyph();
		FontInformation 			fontinfo = component.getFontInformation();
		
		if (glyph != null) { 	    
			int ppem  = component.getPPEM();
			int upem  = fontinfo.getUnitsPerEM();
			
			/* glyphID weergeven */
			g2d.setColor(Color.LIGHT_GRAY);
			g2d.drawString(Integer.toString(glyph.getID()) + " " + glyph.getType(), 0, 10);
			g2d.setColor(Color.BLACK);
					
			/*
			 * Doe een transformatie op de user-coordinaten ruimte.
			 * De oorsprong zal linksonder zijn, ipv linksboven.  Dit is nodig omdat de coordinaten
			 * van de punten opgeslagen zijn tov linksonder.
			 * De user-coordinaten ruimte wordt ook nog opgeschoven zodat de negatieve coordinaten ook mooi
			 * kunnen getoond worden. 
			 */
			AffineTransform flipOverX = new AffineTransform(1, 0, 0, -1, 0, 0);
			AffineTransform translate = 
				AffineTransform.getTranslateInstance(
						Math.abs(Convert.funitsToPixel(fontinfo.getXMin(), ppem, upem)), 
						component.getHeight() - Math.abs(Convert.funitsToPixel(fontinfo.getYMin(), ppem, upem)));
					
			translate.concatenate(flipOverX);
						
			AffineTransform old = g2d.getTransform();
			g2d.transform(translate);

			/*
			 * Draw lines
			 */
			g2d.setColor(Color.WHITE);
			g2d.drawLine(0, 0, component.getWidth(), 0);
			g2d.drawLine(0, 0, 0, component.getHeight());
			g2d.setColor(Color.BLACK);
				
			/*
			 * Draw de glyph
			 *  
			 * Om de contouren van de glyphs te tekenen kunnen we een 'path' van vormen maken
			 * We zullen per contour alle punten overlopen en bepalen welke vorm we op het
			 * path moeten plaatsen ( lijn of bezier-curve ).
			 * Bij het begin van iedere contour zullen we de 'path-pointer' verplaatsen naar het eerste punt 
			 * van die contour.
			 * 
			 */
			Contour 	currentContour;
			
			model.Point startPointOfContour;
			model.Point startPoint;
			model.Point controlPoint;
			model.Point interpolatedPoint = null;
			model.Point endPoint = null;
			
			GeneralPath path = new GeneralPath();
			
			Vector contours = glyph.getContours();
			Iterator itContours = contours.iterator();
			
			while (itContours.hasNext()) {
			
				currentContour = (Contour)itContours.next();
				
				Vector points = currentContour.getPoints();
				Iterator itPoints = points.iterator();
							
				startPointOfContour = (model.Point)itPoints.next();
				startPoint = startPointOfContour;
				
				boolean done = false;
				
				path.moveTo(Convert.funitsToPixel((int)startPointOfContour.getX(), ppem, upem), 
							Convert.funitsToPixel((int)startPointOfContour.getY(), ppem, upem));
				
				if (itPoints.hasNext())
					endPoint = (model.Point)itPoints.next();
				else
					done = false;
								
				while (!done) {

					// De punten weergeven
				    if (_drawPoints) {
					    drawPoint(startPoint, ppem, upem, startPoint.equals(_selectedPoint), g2d);
					}
					
					if (startPoint.isOncurve() && endPoint.isOncurve()) {
						path.lineTo(Convert.funitsToPixel((int)endPoint.getX(), ppem, upem), 
									Convert.funitsToPixel((int)endPoint.getY(), ppem, upem));	
					}
					else {
						while (!endPoint.isOncurve() && (endPoint != startPointOfContour)) {
							
							controlPoint = endPoint;
							
							if (itPoints.hasNext()) {
								endPoint = (model.Point)itPoints.next();
							}
							else {	
								endPoint = startPointOfContour;
							}
							
							if (!endPoint.isOncurve()) {
								interpolatedPoint = Mathematics.interpolate(controlPoint, endPoint, 0.5);
								
								path.quadTo(Convert.funitsToPixel((int)controlPoint.getX(), ppem, upem),
											Convert.funitsToPixel((int)controlPoint.getY(), ppem, upem),
											Convert.funitsToPixel((int)interpolatedPoint.getX(), ppem, upem),
											Convert.funitsToPixel((int)interpolatedPoint.getY(), ppem, upem));
		
								// De punten weergeven
								if (_drawPoints) {
									drawPoint(controlPoint, ppem, upem, controlPoint.equals(_selectedPoint), g2d);
								}
							}
							else {
								path.quadTo(Convert.funitsToPixel((int)controlPoint.getX(), ppem, upem),
										Convert.funitsToPixel((int)controlPoint.getY(), ppem, upem),
										Convert.funitsToPixel((int)endPoint.getX(), ppem, upem),
										Convert.funitsToPixel((int)endPoint.getY(), ppem, upem));
								
								// De punten weergeven
								if (_drawPoints) {								
								    drawPoint(controlPoint, ppem, upem, controlPoint.equals(_selectedPoint), g2d);
								}
							}
						}
					}
					startPoint = endPoint;
					
					if (itPoints.hasNext()) {
						endPoint = (model.Point)itPoints.next();
					}
					else {
						done = true;
					}
					
					// De punten weergeven
					if (_drawPoints) {								
					    drawPoint(endPoint, ppem, upem, endPoint.equals(_selectedPoint), g2d);
					}				
				}
				path.closePath();
				g2d.draw(path);
			}
		}
    }
    
    private void drawPoint(model.Point p, int ppem, int upem, boolean selected, Graphics2D g2d) {
	    
	    int xPosInPixels = Convert.funitsToPixel((int)p.getX(), ppem, upem);
	    int yPosInPixels = Convert.funitsToPixel((int)p.getY(), ppem, upem);
	    
	    if (p.isOncurve()) {
	        g2d.setColor(Color.RED);
	    }
	    else {
	        g2d.setColor(Color.BLUE);
	    }
	    
	    if (selected) {
	        g2d.drawOval(xPosInPixels-OVAL_SIZE/2, yPosInPixels-OVAL_SIZE/2,
	                	 OVAL_SIZE, OVAL_SIZE);    
	    }
	    else {
	        g2d.drawRect(xPosInPixels-RECTANGLE_SIZE/2, yPosInPixels-RECTANGLE_SIZE/2,
	                	 RECTANGLE_SIZE, RECTANGLE_SIZE);    	 
	    }
	    
	    g2d.setColor(Color.BLACK);
	}
}

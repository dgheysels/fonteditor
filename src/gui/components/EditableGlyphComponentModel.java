/*
 * Created on 2-mei-2005
 */
package gui.components;

import model.*;

/**
 * @author Dimitri Gheysels
 */
public class EditableGlyphComponentModel extends AbstractGlyphComponentModel {

    private boolean _modified;
            
    public EditableGlyphComponentModel(FontModel fontmodel/*FontInformation fontinfo*/) {
        super(fontmodel/*fontinfo*/);
    }
     
    /*
	 * De gewijzigde glyph wordt nu in het fontmodel opgeslagen.  
	 */
	public void save() { 	     
	    try {
	        _glyph.setModified(true);
	        _fontmodel.setGlyph(_glyph);    	        
	    }
	    catch(Exception ex) {
	        ex.printStackTrace();
	    }       
	}
    
    /*public Glyph getGlyph() {
        return _glyph;
    }*/
    
    /*
     * GETTERS / SETTERS
     */
    public boolean isModified() {
        return _modified;
    }
    
    public void setModified(boolean v) {
        _modified = v;
    }
}

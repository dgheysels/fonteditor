/*
 * Created on 2-mei-2005
 */
package gui.components;

import gui.IGUIConstants;

import javax.swing.UIManager;

/**
 * @author Dimitri Gheysels
 */
public class ReadOnlyGlyphComponent extends AbstractGlyphComponent implements IGUIConstants {

    private ReadOnlyGlyphComponentModel _model;
    
    /*
	 * CONSTRUCTORS
	 */
    public ReadOnlyGlyphComponent() {}
    
	public ReadOnlyGlyphComponent(ReadOnlyGlyphComponentModel model) {
	    _model = model;
	    setPointSize(LARGEGLYPHSIZE);
	}
	
	public AbstractGlyphComponentModel getModel() { return _model; }
	public void setModel(ReadOnlyGlyphComponentModel model) { _model = model; }
	
	/*
	 * Methods voor ComponentUI-delegate
	 * Deze worden gebruikt door het Swing-Framework
	 */
	public void setUI(ReadOnlyGlyphComponentUI componentUI) {
		super.setUI(componentUI);
	}
	
	public void updateUI() {
		setUI((ReadOnlyGlyphComponentUI)UIManager.getUI(this));
		invalidate();
	}
	
	public String getUIClassID() {
		return "ReadOnlyGlyphComponentUI";
	}
}

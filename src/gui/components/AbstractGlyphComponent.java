/*
 * Created on 2-mei-2005
 */
package gui.components;

import java.awt.Dimension;
import javax.swing.JComponent;

import model.*;

/**
 * @author Dimitri Gheysels
 */
public abstract class AbstractGlyphComponent extends JComponent {
     
    public abstract AbstractGlyphComponentModel getModel();
    
    public void initialize() {
	    setSize();
	}
    
    /*
     * GETTERS / SETTERS
     */
   
    public FontInformation getFontInformation() {
        return getModel().getFontInformation();
    }
    
    public void setFontInformation(FontInformation f) {
        getModel().setFontInformation(f);
    }
    
    public void setPointSize(int pointSize) {
	    getModel().setPointSize(pointSize);
	    setSize();	
	}
	
    public void setGlyph(Glyph g) {
	    getModel().setGlyph(g);
	}
    
	public int getPointSize() {
	    return getModel().getPointSize();
	}
	
	public int getPPEM() {
	    return getModel().getPPEM();
	}
	
	public Glyph getGlyph() {
	    return getModel().getGlyph();
	}
	
	protected void setSize() {
		
		//de lengte en breedte van het component zijn gelijk aan de max. breedte/lengte van het FONT +
		//abs(min breedte/lengte) van het FONT
		int width = extra.Convert.funitsToPixel(
				Math.abs(getModel().getFontInformation().getXMin()) + Math.abs(getModel().getFontInformation().getXMax()), 
				getModel().getPPEM(), 
				getModel().getFontInformation().getUnitsPerEM());
		
		int height = extra.Convert.funitsToPixel(
				Math.abs(getModel().getFontInformation().getYMin()) + Math.abs(getModel().getFontInformation().getYMax()), 
				getModel().getPPEM(),
				getModel().getFontInformation().getUnitsPerEM());
		
		setMinimumSize(new Dimension(width, height));
		setPreferredSize(new Dimension(width, height));
		//JComponent setSize-method
		setSize(new Dimension(width, height));
	
		updateUI();
	}
}

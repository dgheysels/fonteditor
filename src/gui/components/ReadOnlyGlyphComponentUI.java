/*
 * Created on 2-mei-2005
 */
package gui.components;

import java.awt.*;
import javax.swing.JComponent;
import javax.swing.plaf.ComponentUI;

/**
 * @author Dimitri Gheysels
 */
public class ReadOnlyGlyphComponentUI extends AbstractGlyphComponentUI {

    public static ComponentUI createUI(JComponent c) {
		return new ReadOnlyGlyphComponentUI();
	}
    
	public void installUI(JComponent c) {
		ReadOnlyGlyphComponent glyphComponent = (ReadOnlyGlyphComponent)c;
	}
	
	public void uninstallUI(JComponent c) {
		ReadOnlyGlyphComponent glyphComponent = (ReadOnlyGlyphComponent)c;
	}
    
    public void paint(Graphics g, JComponent c) {
        super.paint((Graphics2D)g, (AbstractGlyphComponent)c);
    }
}

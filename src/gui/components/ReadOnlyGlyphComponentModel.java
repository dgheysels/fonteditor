/*
 * Created on 2-mei-2005
 */
package gui.components;

import model.FontModel;

/**
 * @author Dimitri Gheysels
 */
public class ReadOnlyGlyphComponentModel extends AbstractGlyphComponentModel {

    public ReadOnlyGlyphComponentModel(FontModel fontmodel) {
        super(fontmodel);
    } 
}

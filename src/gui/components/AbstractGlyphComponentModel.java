/*
 * Created on 2-mei-2005
 */
package gui.components;

import model.*;
import extra.ScreenProperties;

/**
 * @author Dimitri Gheysels
 */
public abstract class AbstractGlyphComponentModel {

    protected int   _pointSize;
	protected int   _ppem;
		
	protected Glyph     _glyph;
	protected FontModel _fontmodel;
	//protected FontInformation _fontinformation;
	
	public AbstractGlyphComponentModel(FontModel fontmodel/*FontInformation fontinfo*/) {
	    _fontmodel = fontmodel;
	    //_fontinformation = fontinfo;
	}
	
	/*
	 * GETTERS / SETTERS
	 */
	public Glyph getGlyph() { return _glyph; }
	public int   getPointSize() { return _pointSize; }
	public int   getPPEM() { return _ppem; }
	public FontInformation getFontInformation() { return /*_fontinformation;*/_fontmodel.getFontInformation(); }
			
	public void setPointSize(int pointSize) { 
		_pointSize = pointSize;
		calculatePPEM();
	}
	
	public void setGlyph(Glyph g) { _glyph = g; }
	public void setFontInformation(FontInformation info) { /*_fontinformation = info;*/_fontmodel.setFontInformation(info); }
	
	private void calculatePPEM() {
		_ppem = (int)(_pointSize * (ScreenProperties.getResolution()/72.0));
	}
}

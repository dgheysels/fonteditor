/*
 * Created on 25-apr-2005
 */
package gui;

public interface IGUIConstants {

    int COLUMNCOUNT = 8;
    int SMALLGLYPHSIZE = 36;
    int LARGEGLYPHSIZE = 300;
    
    /*
     * Deze constanten worden gebruikt om de vierkantjes en circeltjes te tekenen die resp. punten
     * en geselecteerde punten voorstellen.  PRECISION duidt aan hoeveel funits je verkeerd mag klikken
     * zodat je toch nog het punt geselecteerd hebt
     */
    int PRECISION      = 5;
    int OVAL_SIZE      = 7;
    int RECTANGLE_SIZE = 3;
}

/*
 * Created on 6-apr-2005
 */
package gui;

import java.util.*;
import javax.swing.table.*;

import model.*;

public class GlyphTableModel extends AbstractTableModel implements Observer {

	private Glyph[][] _glyphs;
    private int       _columnCount;
	private FontModel _fontmodel;
	
	public GlyphTableModel(FontModel fontmodel, int columnCount) {
		
	    _fontmodel = fontmodel;
		_columnCount = columnCount;		
		
		Glyph[] data = fontmodel.getGlyphs();
		
		// van 1-dimensionale table naar 2-dimensionale table
		_glyphs = new Glyph[(int)Math.ceil(data.length / (double)_columnCount)]
		                            [_columnCount];
		
		_glyphs = extractGlyphs(data);
	}
	
	public int getRowCount() {
		return _glyphs.length;
	}

	public int getColumnCount() {
		return _glyphs[0].length;
	}
	
	public Class getColumnClass(int c) { return Glyph.class; }

    public Object getValueAt(int row, int column) {
		return _glyphs[row][column];
	}
	
	public void setValueAt(Object value, int row, int column) {
	    _glyphs[row][column] = (Glyph)value;
	}
		
	public FontModel getFontModel() {
	    return _fontmodel;
	}
	
	private Glyph[][] extractGlyphs(Glyph[] data) {
	    
	    for (int i=0; i<data.length; i++) {
			int rowIndex = i/_columnCount;
			int columnIndex = i - (rowIndex * _columnCount); 
			
			_glyphs[rowIndex][columnIndex] = data[i];
		}
	    
	    return _glyphs;
	}
	
	public void update(Observable subject, Object arg) {
        
       _glyphs = extractGlyphs(((FontModel)subject).getGlyphs()); 
       fireTableDataChanged();
    }
}

/*
 * Created on 14-apr-2005
 */
package gui;

import java.io.File;

import javax.swing.filechooser.FileFilter;

/**
 * Filter to retrieve only .TTF files.  JFileChooser uses this.
 * 
 * @author Dimitri Gheysels
 */
public class FontFileFilter extends FileFilter {
	
	public boolean accept(File file) {
		
		if (file != null) {
			if (file.isDirectory()) {
				return true;
			}
			else {
				String extension = getExtension(file);
				if (extension != null && extension.equals("ttf")) {
					return true;
				}
			
				return false;
			}
		}
		else {
			return false;
		}
	}
		
	public String getExtension(File file) {
		
		if (file != null) {
		    
			String filename = file.getName();
		    int index = filename.lastIndexOf('.');
		    
		    if (index > 0 && index < filename.length()-1) {
		    	return filename.substring(index+1).toLowerCase();
		    }
		    else {
		    	return null;
		    }
		}
		else {
			return null;
		}
	}
	
	public String getDescription() {
		return "OpenType fonts (*.ttf)";
	}
}

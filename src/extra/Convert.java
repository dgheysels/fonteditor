/*
 * Created on 31-mrt-2005
 */
package extra;

import java.io.UnsupportedEncodingException;

public class Convert {

	public static String byteToString16BE(byte[] stringdata) throws UnsupportedEncodingException {
		return new String(stringdata, "UTF-16BE");
	}
	
	public static int funitsToPixel(int funit, int ppem, int upem) {
		return funit * ppem/upem;
	}
	
	public static int pixelToFUnit(int pixel, int ppem, int upem) {
		return pixel * upem/ppem;
	}
}

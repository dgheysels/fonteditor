/*
 * Created on 14-apr-2005
 */
package extra;

import java.awt.Dimension;
import java.awt.Toolkit;

public class ScreenProperties {
	
	public static int getResolution() {
		return Toolkit.getDefaultToolkit().getScreenResolution();
	}
	
	public static Dimension getScreenSize() {
		return Toolkit.getDefaultToolkit().getScreenSize();
	}
	
	
}

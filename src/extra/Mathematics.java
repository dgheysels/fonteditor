/*
 * Created on 30-mrt-2005
 */
package extra;

import model.*;

/**
 * @author Dimitri Gheysels
 */
public class Mathematics {

	public static Point interpolate(Point a, Point b, double t) {
		
		int x = (int) (a.getX() + t*(b.getX() - a.getX()));
		int y = (int) (a.getY() + t*(b.getY() - a.getY()));
		
		return new Point(-1, x, y, true);	
	}
}

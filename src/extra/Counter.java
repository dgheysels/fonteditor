/*
 * Created on 29-mrt-2005
 */
package extra;

/**
 * Numbergenerator.  Especially used for numbering the glyphs
 * 
 * @author Dimitri Gheysels
 */
public class Counter {

	private static int _value = 0;
	
	public static void reset() { _value = 0; }
	public static void set(int value) { _value = value; }
	public static int  get() { return _value++; }
	
}

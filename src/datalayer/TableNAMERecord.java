/*
 * Created on 21-feb-2005
 */
package datalayer;

class TableNAMERecord {
	
	private int _platformID;
	private int _encodingID;
	private int _languageID;
	private int _nameID;
	private int _length;
	private int _offset;
	private byte[] _data;
		
	public int getEncodingID() { return _encodingID; }
	public int getLanguageID() { return _languageID; }
	public int getLength() { return _length; }
	public int getNameID() { return _nameID; }
	public int getOffset() { return _offset; }
	public int getPlatformID() { return _platformID; }
	public byte[] getData() { return _data; }
		
	public void setEncodingID(int encodingID) { _encodingID = encodingID; }
	public void setLanguageID(int languageID) {	_languageID = languageID; }
	public void setLength(int length) { _length = length; }
	public void setNameID(int _nameid) { _nameID = _nameid; }
	public void setOffset(int offset) { _offset = offset; }
	public void setPlatformID(int platformID) { _platformID = platformID; }
	public void setData(byte[] data) { _data = data; }
	
	public String toString() {
	    return "\t> " + _platformID + " " + _encodingID + " " + new String(_data);
	}
}

/*
 * Created on 22-feb-2005
 */
package datalayer;

class TableCMAPFormat2Subheader {

	private int _firstCode;
	private int _entryCount;
	private short _idDelta;
	private int   _idRangeOffset;
	
	public int getEntryCount() { return _entryCount; }
	public int getFirstCode() { return _firstCode; }
	public short getIdDelta() { return _idDelta; }
	public int getIdRangeOffset() { return _idRangeOffset; }
	
	public void setEntryCount(int entryCount) { _entryCount = entryCount; }
	public void setFirstCode(int firstCode) { _firstCode = firstCode; }
	public void setIdDelta(short idDelta) { _idDelta = idDelta; }
	public void setIdRangeOffset(int idRangeOffset) { _idRangeOffset = idRangeOffset; }
}

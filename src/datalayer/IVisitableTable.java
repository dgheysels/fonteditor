/*
 * Created on 20-feb-2005
 */
package datalayer;

import java.io.IOException;

/**
 * @author Dimitri Gheysels
 */
interface IVisitableTable {
	void accept(TableVisitor v) throws IOException;
}

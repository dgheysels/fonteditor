/*
 * Created on 24-feb-2005
 */
package datalayer;

import java.io.IOException;
import java.util.*;

import model.CompositeGlyph;
import model.Glyph;

class TableGLYFComposite extends TableGLYF {

	private Vector _components = new Vector();
	
	public static final int ARG_1_AND_2_ARE_WORDS    = 1;
	public static final int ARGS_ARE_XY_VALUES       = 2;
	public static final int ROUND_XY_TO_GRID	     = 4;
	public static final int WE_HAVE_A_SCALE		     = 8;
	public static final int MORE_COMPONENTS		     = 32;
	public static final int WE_HAVE_AN_X_AND_Y_SCALE = 64;
	public static final int WE_HAVE_A_TWO_BY_TWO     = 128;
	public static final int WE_HAVE_INSTRUCTIONS     = 256;
	public static final int USE_MY_METRICS	         = 512;
	public static final int OVERLAP_COMPOUNT	     = 1024;
	public static final int SCALED_COMPONENT_OFFSET  = 2048;
	public static final int UNSCALED_COMPONENT_OFSET = 4096;
	
	public TableGLYFComposite(short numContours) {
		_numContours = numContours;
	}
	
	public int[] getInstructions() { return _instructions; }
	public int getNumInstructions() { return _numInstructions; }
	public Vector getComponents() { return _components; }
	
	public void setInstructions(int[] instructions) { _instructions = instructions; }
	public void setNumInstructions(int numInstructions) {	_numInstructions = numInstructions; }
	
	public Glyph getAsConceptualGlyph() {

	    CompositeGlyph glyph = new CompositeGlyph();
			
		Vector   	  components = getComponents();
		GlyfComponent currentComponent;
		Glyph         currentGlyph;
		Iterator      it       	 = components.iterator();
		glyph               	 = new CompositeGlyph();
			
		// alle componenten van een composite glyph overlopen en de
		// corresponderen simple-glyph opzoeken
		while (it.hasNext()) {				
			currentComponent = (GlyfComponent)it.next();
			currentGlyph     = FontManager.getInstance().getGlyph(currentComponent.getGlyphIndex());
				
			if (currentGlyph != null) {
				// het component moet nog bewerkt (translatie, scale) worden
				int arg1;
				int arg2;
				int flags = currentComponent.getFlags();
					
				if ((flags & TableGLYFComposite.ARGS_ARE_XY_VALUES) != 0) {
					arg1 = currentComponent.getArgument1();
					arg2 = currentComponent.getArgument2();
					
					if ((flags & TableGLYFComposite.WE_HAVE_AN_X_AND_Y_SCALE) != 0) {
						currentGlyph.scale(currentComponent.getXScale().toDouble(), 
										   currentComponent.getYScale().toDouble(),
										   arg1,
										   arg2);
					}
					else if ((flags & TableGLYFComposite.WE_HAVE_A_SCALE) != 0) {
						currentGlyph.scale(currentComponent.getScale().toDouble(),
										   currentComponent.getScale().toDouble(),
										   arg1,
										   arg2);
					}
					else if ((flags & TableGLYFComposite.WE_HAVE_A_TWO_BY_TWO) != 0) {
						currentGlyph.scale2by2(currentComponent.getScale10().toDouble(),
										       currentComponent.getScale01().toDouble(),
										       arg1,
										       arg2);
					}
					else {
						currentGlyph.translate(arg1, arg2);
					}	
				}
				glyph.addGlyphComponent(currentGlyph);
			}		
		}
		
		return glyph;
	}
	
	public void  setFromConceptualGlyph(Glyph g) {
	    // Composite glyphs kunnen we nog niet wijzigen -> geen transform nodig
	}
	
	public void accept(TableVisitor v) throws IOException {
		v.visitTableGLYFComposite(this);
	}
}

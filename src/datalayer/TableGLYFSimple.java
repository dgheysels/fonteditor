/*
 * Created on 24-feb-2005
 */
package datalayer;

import java.io.IOException;
import java.util.ListIterator;
import java.util.Vector;

import extra.Counter;

import model.Contour;
import model.Glyph;
import model.Point;
import model.SimpleGlyph;

class TableGLYFSimple extends TableGLYF {

	private int[] _endPtsOfContours;
	private int   _instructionLength;
	
	private int[] _flags;
	
	private int[] _xCoordinates;
	private int[] _yCoordinates;
	
	public static final int ONCURVE = 1;
	public static final int BYTE_X  = 2;
	public static final int BYTE_Y  = 4;
	public static final int REPEAT  = 8;
	public static final int SAME_X  = 16;
	public static final int SAME_Y  = 32;
			
	public TableGLYFSimple(short numContours) {
		_numContours = numContours;
	}
	
	public int[] getEndPtsOfContours() { return _endPtsOfContours; }
	public int[] getFlags() { return _flags; }
	public int getInstructionLength() {	return _instructionLength; }
	public int[] getInstructions() { return _instructions; }
	public int[] getXCoordinates() { return _xCoordinates; }
	public int[] getYCoordinates() { return _yCoordinates; }
	
	public void setEndPtsOfContours(int[] endPtsOfContours) { _endPtsOfContours = endPtsOfContours; }
	public void setFlags(int[] flags) { _flags = flags; }
	public void setInstructionLength(int instructionLength) { _instructionLength = instructionLength; }
	public void setInstructions(int[] instructions) { _instructions = instructions; }
	public void setXCoordinates(int[] xCoordinates) { _xCoordinates = xCoordinates; }
	public void setYCoordinates(int[] yCoordinates) { _yCoordinates = yCoordinates; }
	
	public void accept(TableVisitor v) throws IOException {
		v.visitTableGLYFSimple(this);
	}
	
	public Glyph getAsConceptualGlyph() {
	    
	    SimpleGlyph glyph = new SimpleGlyph();
			
		int[] xCoordinates 		  = getXCoordinates();
		int[] yCoordinates 		  = getYCoordinates();
		int[] flags        		  = getFlags();
		int[] endPointsOfContours = getEndPtsOfContours();
		int   numContours         = getNumContours();
		int   startIndex          = 0;
		
		int   currentXCo   = 0;
		int   currentYCo   = 0;
		int   pointNumber;
		
		Contour contour;
		Point   point;
							
		glyph.setXMin(getXMin());
		glyph.setYMin(getYMin());
		glyph.setXMax(getXMax());
		glyph.setYMax(getYMax());
			
		for (int i=0; i<numContours; i++) {
				
			// Voor iedere contour maken we een contour-object waarin de bijhorende punten
			// in opgeslagen zijn.
			contour = new Contour();
									
			for (int j=startIndex; j<=endPointsOfContours[i]; j++) {			
				// volgende pointid nemen
				pointNumber = Counter.get();						
					
				/*
				 * Coordinaten absoluut maken
				 *  currentXCo en currentYCo zetten we initieel op de coordinaten 
				 *   van het eerste punt
				 *  vervolgens worden de vectoren ( coordinaten in file ) 
				 *   opgeteld bij de current-coordinaten
				 *    --> current-coordinaten zijn absoluut geworden
				 */						
				if (pointNumber != 0) {
					currentXCo += xCoordinates[j];
					currentYCo += yCoordinates[j];
				}
				else {
					currentXCo = xCoordinates[j];
					currentYCo = yCoordinates[j];
				}
				
				point = new Point(pointNumber, currentXCo, currentYCo, false);
				
				// punt = onCurve ?
				if ( (flags[j] & TableGLYFSimple.ONCURVE) != 0)
					point.setOncurve(true);
																				
				// punt toevoegen aan de contour
				contour.addPoint(point);
			}										
														
			// nieuwe contour beginnen
			startIndex = endPointsOfContours[i]+1;
			glyph.addContour(contour);
		}			
		
		//glyph.setID(glyphID);			
	return glyph;
	}
	
	 /*
     * 1. Neem alle punten uit de Glyph en overloop ze per contour.  BEGIN ACHTERAAN !
     * 2. Maak de coordinaten opnieuw relatief en bereken de flags
     * 3. Zet de relatieve coordinaten + flags in de TableGLYF
     * 4. Bepaal de endPointOfContour
     * 5. Zet de tableGLYF terug in de _tables collection  
     */
	public void  setFromConceptualGlyph(Glyph g) {
	    
	    g = (SimpleGlyph)g;
	    
        int totalPoints   = g.getTotalPoints();
        int totalContours = g.getTotalContours();
        
        // onderscheid maken tussen de eerste contour en de rest. 
        boolean lastContour        = true;	 
        boolean lastPointOfContour = true;
        
        Vector       contours   = g.getContours();
        Vector       points;
        ListIterator contoursIt = contours.listIterator(totalContours);
        ListIterator pointsIt;
        
        Contour      currentContour;
        Point		 currentPoint  = null;
        Point 		 previousPoint = null;
       	        
        int[] xCo = new int[totalPoints];
        int[] yCo = new int[totalPoints];
        int[] flags = new int[totalPoints];
        int[] endPointsOfContour = new int[totalContours];
        
        int relativeX;
        int relativeY;
        int flag = 0;
        int pointIndex = totalPoints - 1;
        int contourIndex = totalContours - 1;
        
        while (contoursIt.hasPrevious()) {     	            
            currentContour = (Contour)contoursIt.previous();
            
            points   = currentContour.getPoints();
            pointsIt = points.listIterator(points.size());
            
            // begin van de laatste contour: previouspoint = eerste punt    
            if (pointsIt.hasPrevious() && lastContour) {
                previousPoint = (Point)pointsIt.previous();      
            }
     
            lastPointOfContour = true;          
        
            /*
             * tijdens het overlopen van de contour, schuiven we previous en current
             * steeds eentje op
             */
            while (pointsIt.hasPrevious()) {       
                currentPoint = previousPoint;
                previousPoint = (Point)pointsIt.previous();
                
                // laatste punt van een contour -> endPointsOfContour aanpassen
                if (lastPointOfContour) {
                    if (lastContour)  {
                        endPointsOfContour[contourIndex] = currentPoint.getID();
                        lastContour = false;
                    }
                    else {
                        endPointsOfContour[contourIndex] = previousPoint.getID();
                    }
                    
                    lastPointOfContour = false;
                    contourIndex--;
                }
                
                // relatieve coordinaten berekenen
                relativeX = (int)(currentPoint.x - previousPoint.x);
                relativeY = (int)(currentPoint.y - previousPoint.y);
                flag = calculateFlags(relativeX, relativeY, currentPoint.isOncurve());
                
                xCo[pointIndex] = relativeX;
                yCo[pointIndex] = relativeY;
                flags[pointIndex] = flag;
                	                
                pointIndex--;    
            }    
        }
      
        // alle contouren zijn gedaan: het eerste punt = absoluut
        relativeX = (int)(previousPoint.x);
        relativeY = (int)(previousPoint.y);
        flag = calculateFlags(relativeX, relativeY, previousPoint.isOncurve());
        
        xCo[0] = relativeX;
        yCo[0] = relativeY;
        flags[0] = flag;
       
        setXCoordinates(xCo);
        setYCoordinates(yCo);
        setFlags(flags);
        setEndPtsOfContours(endPointsOfContour);     
    }    
	
	private int calculateFlags(int relativeX, int relativeY, boolean oncurve) {
	    
	    int flag = 0;
	    
	    if (oncurve) {
	        flag |= TableGLYFSimple.ONCURVE;
	    }
	    
	    // X
	    if ((Math.abs(relativeX) < 256) && (relativeX != 0)) {
	        // flags < 256 (past in 1 byte) en (Xco != previousCo (relativeX != 0))
	        flag |= TableGLYFSimple.BYTE_X;
	        
	        // if BYTE_X is set, SAME_X bepaalt het teken
	        if (relativeX > 0) {
	            flag |= TableGLYFSimple.SAME_X;
	        }   
	    }
	    else if (relativeX == 0) {
	        flag |= TableGLYFSimple.SAME_X;
	    }
	    
	    // Y
	    if ((Math.abs(relativeY) < 256) && (relativeY != 0)) {
	        flag |= TableGLYFSimple.BYTE_Y;
	        
	        if (relativeY > 0) {
	            flag |= TableGLYFSimple.SAME_Y;
	        }
	    }
	    else if (relativeY == 0) {
	        flag |= TableGLYFSimple.SAME_Y;
	    }
	    
	    return flag;
	}
}

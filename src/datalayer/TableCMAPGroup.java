/*
 * Created on 24-feb-2005
 */
package datalayer;

import java.io.IOException;

class TableCMAPGroup implements IVisitableTable {
	
	private long _startCharCode;
	private long _endCharCode;
	private long _startGlyphID;
	
	public long getEndCharCode() { return _endCharCode; }
	public long getStartCharCode() { return _startCharCode; }
	public long getStartGlyphID() { return _startGlyphID; }
	
	public void setEndCharCode(long endCharCode) {_endCharCode = endCharCode; }
	public void setStartCharCode(long startCharCode) {_startCharCode = startCharCode; }
	public void setStartGlyphID(long startGlyphID) { _startGlyphID = startGlyphID; }
	
	public void accept(TableVisitor v) throws IOException {
		v.visitTableCMAPGroup(this);
	}
}

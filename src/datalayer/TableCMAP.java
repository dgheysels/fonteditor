/*
 * Created on 22-feb-2005
 */
package datalayer;

import java.io.IOException;
import java.util.*;

class TableCMAP implements IVisitableTable {
	
	private int _version;
	private int _numTables;
	private Vector _cmapEncodingRecords = new Vector();
	private Vector _cmapEncodingTables  = new Vector();
	
	public int getNumTables() {	return _numTables; }
	public int getVersion() { return _version; }
	public Vector getCmapEncodingRecords() { return _cmapEncodingRecords; }
	public Vector getCmapEncodingTables() { return _cmapEncodingTables; }
	
	public void setNumTables(int numTables) { _numTables = numTables; }
	public void setVersion(int version) { _version = version; }
	
	public void addCmapEncodingRecord(TableCMAPEncodingRecord cmapRecord) { 
		_cmapEncodingRecords.add(cmapRecord);
	}
	
	public void addCmapEncodingTable(TableCMAPEncoding encoding) {
		_cmapEncodingTables.add(encoding);
	}
	
	public void accept(TableVisitor v) throws IOException {
		v.visitTableCMAP(this);
	}
}

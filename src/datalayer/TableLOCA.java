/*
 * Created on 21-feb-2005
 */
package datalayer;

import java.io.IOException;

class TableLOCA implements IVisitableTable {

	private long[] _offsets;
		
	public long   getOffset(int i) { return _offsets[i]; }
	public long[] getOffsets() { return _offsets; }
	
	public void   setOffset(int i, long data) { _offsets[i] = data; }
	public void   setOffsets(long[] offsets) { _offsets = offsets; }
	
	public void accept(TableVisitor v) throws IOException {
		v.visitTableLOCA(this);
	}
}

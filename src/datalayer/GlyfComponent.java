/*
 * Created on 24-feb-2005
 */
package datalayer;

import java.io.IOException;

import datatypes.*;

class GlyfComponent implements IVisitableTable {

	private int 	_flags;
	private int 	_glyphIndex;
	private int 	_argument1;
	private int     _argument2;
	private F2dot14 _scale;
	private F2dot14 _xscale;
	private F2dot14 _yscale;
	private F2dot14 _scale01;
	private F2dot14 _scale10;
	
	public int getArgument1() { return _argument1; }
	public int getArgument2() { return _argument2; }
	public int getFlags() { return _flags; }
	public int getGlyphIndex() { return _glyphIndex; }
	public F2dot14 getScale01() { return _scale01; }
	public F2dot14 getScale10() { return _scale10; }
	public F2dot14 getXScale() { return _xscale; }
	public F2dot14 getYScale() { return _yscale; }
	public F2dot14 getScale() { return _scale; }
	
	public void setArgument1(int argument1) { _argument1 = argument1; }
	public void setArgument2(int argument2) { _argument2 = argument2; }
	public void setFlags(int flags) { _flags = flags; }
	public void setGlyphIndex(int glyphIndex) { _glyphIndex = glyphIndex; }
	public void setScale01(F2dot14 scale01) { _scale01 = scale01; }
	public void setScale10(F2dot14 scale10) { _scale10 = scale10; }
	public void setXScale(F2dot14 xscale) { _xscale = xscale; }
	public void setYScale(F2dot14 yscale) { _yscale = yscale; }
	public void setScale(F2dot14 scale) { _scale = scale; }
	
	public void accept(TableVisitor v) throws IOException {
		v.visitTableGLYFCompositeComponent(this);
	}
}

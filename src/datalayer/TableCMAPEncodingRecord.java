/*
 * Created on 22-feb-2005
 */
package datalayer;

class TableCMAPEncodingRecord {

	private int _platformID;
	private int _encodingID;
	private long _offset;
	
	public int getEncodingID() { return _encodingID; }
	public long getOffset() { return _offset; }
	public int getPlatformID() { return _platformID; }
	
	public void setEncodingID(int encodingid) { _encodingID = encodingid; }
	public void setOffset(long offset) { _offset = offset; }
	public void setPlatformID(int platformid) { _platformID = platformid; }
	
	public String toString() {
		return _platformID + " " + _encodingID + " " + _offset;
	}
}

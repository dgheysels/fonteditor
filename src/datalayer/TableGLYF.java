/*
 * Created on 24-feb-2005
 */
package datalayer;

import java.io.IOException;

import model.Glyph;

abstract class TableGLYF implements IVisitableTable {

	protected short _numContours;
	protected short _xMin;
	protected short _yMin;
	protected short _xMax;
	protected short _yMax;
	protected int   _numInstructions;
	protected int[] _instructions;
	
	public short getNumContours() {	return _numContours; }
	public short getXMax() { return _xMax; }
	public short getXMin() { return _xMin; }
	public short getYMax() { return _yMax; }
	public short getYMin() { return _yMin; }
	public int   getNumInstructions() { return _numInstructions; }
	public int[] getInstuctions() { return _instructions; }
	
	public void setNumContours(short numContours) {	_numContours = numContours; }
	public void setXMax(short xMax) { _xMax = xMax; }
	public void setXMin(short xMin) { _xMin = xMin; }
	public void setYMax(short yMax) { _yMax = yMax;	}
	public void setYMin(short yMin) { _yMin = yMin;  }
	public void setNumInstructions(int numInstructions) { _numInstructions = numInstructions; }
	public void setInstructions(int[] instructions) { _instructions = instructions; }
	
	/*
	 * Transformeer een TableGLYF-object naar een 'hoog-niveau' glyph
	 */
	public abstract Glyph getAsConceptualGlyph();
	
	/*
	 * Een 'hoog-niveau' glyph terug als een TableGLYF-object transformeren
	 */
	public abstract void  setFromConceptualGlyph(Glyph g);
	
	
	public abstract void accept(TableVisitor v) throws IOException; 	
}

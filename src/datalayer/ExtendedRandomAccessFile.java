package datalayer;

import java.io.*;
import datatypes.*;

/**
 *This class extends the standard JDK1.4.2 RandomAccessFile with some new methods
 *to read some datatypes definied in de TTF fontfiles
 *<code>readUnsigned32()</code>
 *<code>readFixed()</code> a 32-bit signed fixed-point number (16.16)
 *<code>readF2DOT14</code> a 16-bit signed fixed-point number (2.14)
 *
 * @author  Dimitri Gheysels
 * @version 1.0 17/11/2004
 * 
 */
class ExtendedRandomAccessFile extends RandomAccessFile {
	
	public ExtendedRandomAccessFile(File file, String mode) throws FileNotFoundException {
		super(file, mode);
	}
    
	public ExtendedRandomAccessFile(String filename, String mode) throws FileNotFoundException {
		super(filename, mode);
	}
    
	public final long readUnsigned32() throws IOException {
	        
		long v1 = readUnsignedShort();
		long v2 = readUnsignedShort();
		     
		return (v1 << 16) | v2;
	}
	    
	public final DateTime readDateTime() throws IOException {
	        
		return new DateTime(readLong());
	}
	    
	public final Fixed readFixed() throws IOException {
		        
		byte[] bytes = new byte[4];
		read(bytes, 0, 2);
		read(bytes, 2, 2);
		        
		return new Fixed(bytes);
	}
	    
	public final F2dot14 readF2dot14() throws IOException {
	        
		byte[] bytes = new byte[2];
		bytes[0] = readByte();
		bytes[1] = readByte();
		        
		return new F2dot14(bytes);
	}
	    
	public final void writeUnsigned32(long value) throws IOException {
	        
		byte[] b = new byte[4];
		        
		b[0] = (byte) (value >> 24);
		b[1] = (byte) (value >> 16);
		b[2] = (byte) (value >> 8);
		b[3] = (byte) value;
		        
		write(b);
	}
	    
	public final void writeFixed(Fixed value) throws IOException {
		write(value.getBytes());
	}
	    
	public final void writeF2dot14(F2dot14 value) throws IOException {
		write(value.getBytes());
	}
	    
	public final void writeDateTime(DateTime value) throws IOException {
		writeLong(value.getData());
	}
}
/*
 * Created on 21-feb-2005
 */
package datalayer;

import java.io.IOException;

import datatypes.*;

class TableMAXP implements IVisitableTable {

	private Fixed _tableVersionNumber;
	private int   _numGlyphs;
	private int   _maxPoints;
	private int   _maxContours;
	private int   _maxCompositePoints;
	private int   _maxCompositeContours;
	private int   _maxZones;
	private int   _maxTwilightPoints;
	private int   _maxStorage;
	private int   _maxFunctionDefs;
	private int   _maxInstructionDefs;
	private int   _maxStackElements;
	private int   _maxSizeOfInstructions;
	private int   _maxComponentElements;
	private int   _maxComponentDepth;
	
	public int getMaxComponentDepth() { return _maxComponentDepth;	}
	public int getMaxComponentElements() { return _maxComponentElements; }
	public int getMaxCompositeContours() {	return _maxCompositeContours; }
	public int getMaxCompositePoints() { return _maxCompositePoints; }
	public int getMaxContours() { return _maxContours; }
	public int getMaxFunctionDefs() { return _maxFunctionDefs; }
	public int getMaxInstructionDefs() { return _maxInstructionDefs; }
	public int getMaxPoints() { return _maxPoints; }
	public int getMaxSizeOfInstructions() { return _maxSizeOfInstructions; }
	public int getMaxStackElements() { return _maxStackElements; }
	public int getMaxStorage() { return _maxStorage; }
	public int getMaxTwilightPoints() { return _maxTwilightPoints; }
	public int getMaxZones() { return _maxZones; }
	public int getNumGlyphs() {return _numGlyphs; }
	public Fixed getTableVersionNumber() { return _tableVersionNumber; }
	
	public void setMaxComponentDepth(int maxComponentDepth) {	_maxComponentDepth = maxComponentDepth; }
	public void setMaxComponentElements(int maxComponentElements) { _maxComponentElements = maxComponentElements; }
	public void setMaxCompositeContours(int maxCompositeContours) { _maxCompositeContours = maxCompositeContours; }
	public void setMaxCompositePoints(int maxCompositePoints) { _maxCompositePoints = maxCompositePoints; }
	public void setMaxContours(int maxContours) { _maxContours = maxContours; }
	public void setMaxFunctionDefs(int maxFunctionDefs) { _maxFunctionDefs = maxFunctionDefs; }
	public void setMaxInstructionDefs(int maxInstructionDefs) { _maxInstructionDefs = maxInstructionDefs; }
	public void setMaxPoints(int maxPoints) { _maxPoints = maxPoints; }
	public void setMaxSizeOfInstructions(int maxSizeOfInstructions) { _maxSizeOfInstructions = maxSizeOfInstructions; }
	public void setMaxStackElements(int maxStackElements) { _maxStackElements = maxStackElements; }
	public void setMaxStorage(int maxStorage) { _maxStorage = maxStorage; }
	public void setMaxTwilightPoints(int maxTwilightPoints) { _maxTwilightPoints = maxTwilightPoints; }
	public void setMaxZones(int maxZones) { _maxZones = maxZones; }
	public void setNumGlyphs(int numGlyphs) { _numGlyphs = numGlyphs; }
	public void setTableVersionNumber(Fixed versionNumber) { _tableVersionNumber = versionNumber; }
	
	public void accept(TableVisitor v) throws IOException {	
		v.visitTableMAXP(this);
	}
}

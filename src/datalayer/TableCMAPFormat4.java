/*
 * Created on 22-feb-2005
 */
package datalayer;

import java.io.IOException;

class TableCMAPFormat4 extends TableCMAPEncoding {
	
	private int 	_segCountX2;
	private int 	_searchRange;
	private int 	_entrySelector;
	private int 	_rangeShift;
	private int[]   _endCount;
	private int     _reservedPad;
	private int[]   _startCount;
	private short[] _idDelta;
	private int[]	_idRangeOffset;
	private int[]	_glyphIdArray;
	
	public TableCMAPFormat4() {
		_format = 4;
	}
	
	public int[] getEndCount() { return _endCount; }
	public int getEntrySelector() { return _entrySelector; }
	public int[] getGlyphIdArray() { return _glyphIdArray; }
	public short[] getIdDelta() { return _idDelta; }
	public int[] getIdRangeOffset() { return _idRangeOffset; }
	public int getRangeShift() { return _rangeShift; }
	public int getReservedPad() { return _reservedPad; }
	public int getSearchRange() { return _searchRange; }
	public int getSegCountX2() { return _segCountX2; }	
	public int[] getStartCount() { return _startCount; }
	
	public void setEndCount(int[] endCount) { _endCount = endCount; }
	public void setEntrySelector(int entrySelector) { _entrySelector = entrySelector; }
	public void setGlyphIdArray(int[] glyphIdArray) { _glyphIdArray = glyphIdArray; }
	public void setIdDelta(short[] idDelta) { _idDelta = idDelta; }
	public void setIdRangeOffset(int[] idRangeOffset) { _idRangeOffset = idRangeOffset; }
	public void setRangeShift(int rangeShift) { _rangeShift = rangeShift; }
	public void setReservedPad(int reservedPad) { _reservedPad = reservedPad; }
	public void setSearchRange(int searchRange) { _searchRange = searchRange; }
	public void setSegCountX2(int segCountX2) { _segCountX2 = segCountX2; }
	public void setStartCount(int[] startCount) { _startCount = startCount; }
	
	public void accept(TableVisitor v) throws IOException {
		v.visitTableCMAPFormat4(this);
	}
}

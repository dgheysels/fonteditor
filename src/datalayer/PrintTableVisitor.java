/*
 * Created on 20-feb-2005
 */
package datalayer;

import java.io.IOException;
import java.io.*;
import java.util.*;

class PrintTableVisitor extends TableVisitor {

	private PrintStream _out;
	public int          _glyphCounter = 0;
	
	public PrintTableVisitor(PrintStream out) {
		_out = out;
	}
	
	public void visitTableOffset(TableOffset to) throws IOException {
		_out.println("OFFSETTABLE:");
		_out.println("\t sfnt: " + to.getSfnt());
		_out.println("\t numTables: " + to.getNumTables());
		_out.println("\t searchRange: " + to.getSearchRange());
		_out.println("\t entrySelector: " + to.getEntrySelector());
		_out.println("\t rangeShift: " + to.getRangeShift());
	}

	public void visitTableDirectory(TableDirectory td) throws IOException {
		_out.println("TABLEDIRECTORY:");
		
		SortedMap entries    = td.getTableDirectoryEntries();
		Iterator it = entries.entrySet().iterator();
				
		while (it.hasNext()) {
			_out.println("\t" + ((Map.Entry)it.next()).getValue());
		}	
	}
	
	public void visitTableHEAD(TableHEAD head) throws IOException {
		_out.println("head:");
		_out.println("\ttableVersionNumber: " + head.getTableVersionNumber());
		_out.println("\tfontRevision: " + head.getFontRevision());
		_out.println("\tchecksumAdjustment: " + head.getChecksumAdjustment());
		_out.println("\tmagicNumber: " + Long.toHexString(head.getMagicNumber()));
		_out.println("\tflags: " + head.getFlags());
		_out.println("\tunitsPerEm: " + head.getUnitsPerEm());
		_out.println("\tcreated (in sec): " + head.getCreated());
		_out.println("\tmodified (in sec): " + head.getModified());
		_out.println("\txMin: " + head.getXMin());
		_out.println("\tyMin: " + head.getYMin());
		_out.println("\txMax: " + head.getXMax());
		_out.println("\tyMax: " + head.getYMax());
		_out.println("\tmacStyle: " + head.getMacStyle());
		_out.println("\tlowestRecPPEM: " + head.getLowestRecPPEM());
		_out.println("\tfontDirectionHint: " + head.getFontDirectionHint());
		_out.println("\tindexToLocFormat: " + head.getIndexToLocFormat());
		_out.println("\tglyphDataFormat: " + head.getGlyphDataFormat());
	}
	
	public void visitTableMAXP(TableMAXP maxp) throws IOException {
		_out.println("maxp: ");
		_out.println("\ttableVersionNumber: " + maxp.getTableVersionNumber());
		_out.println("\tnumGlyphs: " + maxp.getNumGlyphs());
		_out.println("\tmaxPoints: " + maxp.getMaxPoints());			
		_out.println("\tmaxContours: " + maxp.getMaxContours());
		_out.println("\tmaxCompositePoints: " + maxp.getMaxCompositePoints());
		_out.println("\tmaxZones: " + maxp.getMaxZones());
		_out.println("\tmaxTwilightPoints: " + maxp.getMaxTwilightPoints());
		_out.println("\tmaxStorage: " + maxp.getMaxStorage());
		_out.println("\tmaxFunctionDefs: " + maxp.getMaxFunctionDefs());
		_out.println("\tmaxInstructionDefs: " + maxp.getMaxInstructionDefs());
		_out.println("\tmaxStackElements: " + maxp.getMaxStackElements());
		_out.println("\tmaxSizeOfInstructions: " + maxp.getMaxSizeOfInstructions());
		_out.println("\tmaxComponentElements: " + maxp.getMaxComponentElements());
		_out.println("\tmaxComponentDepth: " + maxp.getMaxComponentDepth());
	}
	
	public void visitTableLOCA(TableLOCA loca) throws IOException {
		_out.println("loca: ");
		
		long[] offsets = loca.getOffsets();
		
		for (int i=0; i<offsets.length; i++) {
			_out.print("\ti:" + i + " = " + offsets[i]);
			if (i%5 == 0) {
				_out.println();				
			}
		}
		
		_out.println();
	}
	
	public void visitTableNAME(TableNAME name) throws IOException {
		_out.println("name: ");
		
		_out.println("\tformat: " + name.getFormat());
		_out.println("\tcount: " + name.getCount());
		_out.println("\tstringOffset: " + name.getStringOffset());
		
		Vector   nameRecords = name.getNameRecords();
		Iterator it          = nameRecords.iterator();
		
		_out.println("\tstrings: ");
		
		while(it.hasNext()) {
			_out.println("\t\t" + it.next());
		}	
	}

	public void visitTableCMAP(TableCMAP cmap) throws IOException {
		_out.println("cmap: ");
		
		_out.println("\tversion: " + cmap.getVersion());
		_out.println("\tnumTables: " + cmap.getNumTables());
		
		Vector   cmapRecords    = cmap.getCmapEncodingRecords();
		Vector   encodingTables = cmap.getCmapEncodingTables();
		Iterator it             = cmapRecords.iterator();
		TableCMAPEncoding curTable;
		
		_out.println("\tencodingRecords: ");
		while(it.hasNext()) {
			_out.println("\t\t" + it.next());
		}	
		
		_out.println("\tencodingTables: ");
		it = encodingTables.iterator();
		while (it.hasNext()) {
			curTable = (TableCMAPEncoding)it.next();
			curTable.accept(this);
		}	
	}
	
	public void visitTableCMAPFormat0(TableCMAPFormat0 format0) throws IOException {
		_out.println();
		_out.println("\t\ttableFormat: " + format0.getFormat());
		_out.println("\t\ttableLength: " + format0.getLength());
		_out.println("\t\ttableLanguage: " + format0.getLanguage());
	}
	
	public void visitTableCMAPFormat2(TableCMAPFormat2 format2) throws IOException {
		_out.println();
		_out.println("\t\ttableFormat: " + format2.getFormat());
		_out.println("\t\ttableLength: " + format2.getLength());
		_out.println("\t\ttableLanguage: " + format2.getLanguage());
		
		int[] subheaderkeys = format2.getSubHeaderKeys();
		
		_out.println("\t\tsubHeaderKeys: ");
		
		for (int i=0; i<subheaderkeys.length; i++) {
			_out.println("\t\t  " + i + ": " + subheaderkeys[i]);
		}
	}
	
	public void visitTableCMAPFormat4(TableCMAPFormat4 format4) throws IOException {
		_out.println();
		_out.println("\t\ttableFormat: " + format4.getFormat());
		_out.println("\t\ttableLength: " + format4.getLength());
		_out.println("\t\ttableLanguage: " + format4.getLanguage());
		
		_out.println("\t\tsegCountX2: " + format4.getSegCountX2());
		_out.println("\t\tsearchRange: " + format4.getSearchRange());
		_out.println("\t\tentrySelector: " + format4.getEntrySelector());
		_out.println("\t\trangeShift: " + format4.getRangeShift());
		_out.println("\t\treservedPad: " + format4.getReservedPad());
		
		int[] startCount = format4.getStartCount();
		int[] endCount   = format4.getEndCount();
		short[] idDelta    = format4.getIdDelta();
		int[] idRangeOffset = format4.getIdRangeOffset();
		
		_out.println("\t\tstartCount\tendCount\tidDelta\tidRangeOffset");
		for (int i=0; i<format4.getSegCountX2()/2; i++) {
			_out.println("\t\t" + startCount[i] + "\t\t" + endCount[i] + "\t\t" + idDelta[i] + "\t\t" + idRangeOffset[i]);
		}
		
		_out.println("\t\tglyphIdArray (length): " + format4.getGlyphIdArray().length);	
	}
	
	public void visitTableCMAPFormat6(TableCMAPFormat6 format6) throws IOException {
		_out.println();
		_out.println("\t\ttableFormat: " + format6.getFormat());
		_out.println("\t\ttableLength: " + format6.getLength());
		_out.println("\t\ttableLanguage: " + format6.getLanguage());
	}
	
	public void visitTableCMAPFormat8(TableCMAPFormat8 format8) throws IOException {
		_out.println();
		_out.println("\t\ttableFormat: " + format8.getFormat());
		_out.println("\t\ttableLength: " + format8.getLength());
		_out.println("\t\ttableLanguage: " + format8.getLanguage());
	}
	
	public void visitTableCMAPFormat10(TableCMAPFormat10 format10) throws IOException {
		_out.println();
		_out.println("\t\ttableFormat: " + format10.getFormat());
		_out.println("\t\ttableLength: " + format10.getLength());
		_out.println("\t\ttableLanguage: " + format10.getLanguage());
	}
	
	public void visitTableCMAPFormat12(TableCMAPFormat12 format12) throws IOException {
		_out.println();
		_out.println("\t\ttableFormat: " + format12.getFormat());
		_out.println("\t\ttableLength: " + format12.getLength());
		_out.println("\t\ttableLanguage: " + format12.getLanguage());
	}
	
	public void visitTableCMAPGroup(TableCMAPGroup group) throws IOException {}
	
	public void visitTableGLYFSimple(TableGLYFSimple simpleGlyf) throws IOException {
		
		_out.println(_glyphCounter++ + "\tsimple glyf: ");
		
		int[] xco = simpleGlyf.getXCoordinates();
		int[] yco = simpleGlyf.getYCoordinates();
		int[] contours = simpleGlyf.getEndPtsOfContours();
		int[] flags = simpleGlyf.getFlags();
		
		_out.println("\t\t xMin: " + simpleGlyf.getXMin() + 
					 " # xMax: " + simpleGlyf.getXMax() +
					 " # yMin: " + simpleGlyf.getYMin() +
					 " # yMax: " + simpleGlyf.getYMax() + 
					 " # numPoints: " + xco.length + 
					 " # numContours: " + contours.length);
		
		_out.println("endpoints of contours: ");
		for (int i=0; i<contours.length; i++) {
		    _out.println((i+1) + ": " + contours[i]);
		}
		
		int startIndex = 0;
		for (int i=0; i<contours.length; i++) {
			_out.println("\t\tContour #" + (i+1) );
			
			for (int j=startIndex; j<=contours[i]; j++) {
				_out.println("\t\t\tP" + (j+1) + " -> dx:" + xco[j] + " dy:" + yco[j] + 
						     (((flags[j] & TableGLYFSimple.ONCURVE) != 0) ? "\t(onCurve) " : "\t\t")  + "\t[ " + (flags[j]) + " ]");
			}
			
			startIndex = contours[i]+1;
		}
		_out.println("------------------------------------------------------------------------------------------------------");
	}
	
	public void visitTableGLYFComposite(TableGLYFComposite compositeGlyf) throws IOException {
		_out.println(_glyphCounter++ + "\tcompositeGLYF: " );
		
		int           componentFlag;
		Vector        components = compositeGlyf.getComponents();
		Iterator      it         = components.iterator();
		GlyfComponent component;
		
		_out.println("\t\t xMin: " + compositeGlyf.getXMin() + 
				 " # xMax: " + compositeGlyf.getXMax() +
				 " # yMin: " + compositeGlyf.getYMin() +
				 " # yMax: " + compositeGlyf.getYMax());
		
		_out.println("\t\tcomponents: ");
		
		while (it.hasNext()) {
			component = (GlyfComponent)it.next();
			
			componentFlag = component.getFlags();
			
			_out.println();
			_out.println("\t\t * glyphid: " + component.getGlyphIndex());
			_out.println("\t\t * arg 1: " + component.getArgument1() + 
					(((componentFlag & TableGLYFComposite.ARGS_ARE_XY_VALUES) != 0) ? " [xy value]" : " [point]"));
			_out.println("\t\t * arg 2: " + component.getArgument2() +
					(((componentFlag & TableGLYFComposite.ARGS_ARE_XY_VALUES) != 0) ? " [xy value]" : " [point]"));
			_out.println("\t\t * scale: " + component.getScale());
			_out.println("\t\t * xscale: " + component.getXScale());
			_out.println("\t\t * yscale: " + component.getYScale());
			_out.println("\t\t * scale01: " + component.getScale01());
			_out.println("\t\t * scale10: " + component.getScale10());
		}
		
		_out.println("------------------------------------------------------------------------------------------------------");
	}
	
	public void visitTableGLYFCompositeComponent(GlyfComponent component) throws IOException {}
	
	public void visitTableOther(TableOther other) throws IOException {
		_out.println("OTHER: " + other.getData().length);		
	}	
}

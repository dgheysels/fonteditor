/*
 * Created on 20-feb-2005
 */
package datalayer;

import java.io.IOException;
import java.util.*;

class LoadTableVisitor extends TableVisitor {

	private ExtendedRandomAccessFile _font;
	
	public LoadTableVisitor(ExtendedRandomAccessFile f) throws IOException {
		_font = f;
	}
		
	public void visitTableOffset(TableOffset to) throws IOException {
		to.setSfnt(_font.readFixed());
		to.setNumTables(_font.readUnsignedShort());
		to.setSearchRange(_font.readUnsignedShort());
		to.setEntrySelector(_font.readUnsignedShort());
		to.setRangeShift(_font.readUnsignedShort());
	}
	
	public void visitTableDirectory(TableDirectory td) throws IOException {
		
		int    entriesSize = ((TableOffset)FontManager.getInstance()
		            			.searchTable("OFFSET")).getNumTables();
		TableDirectoryEntry entry;
		byte[] tag;			
					
		for (int i=0; i<entriesSize; i++) {
			entry = new TableDirectoryEntry();
			
			tag = new byte[4];
			_font.read(tag);
			
			entry.setTag(tag);
			entry.setChecksum(_font.readUnsigned32());
			entry.setOffset(_font.readUnsigned32());
			entry.setLength(_font.readUnsigned32());	
			
			td.addDirectoryEntry(new String(tag), entry);
		}
	}
	
	public void visitTableHEAD(TableHEAD head) throws IOException {
		
		head.setTableVersionNumber(_font.readFixed());
		head.setFontRevision(_font.readFixed());
		head.setChecksumAdjustment(_font.readUnsigned32());
		head.setMagicNumber(_font.readUnsigned32());
		head.setFlags(_font.readUnsignedShort());
		head.setUnitsPerEm(_font.readUnsignedShort());
		head.setCreated(_font.readDateTime());
		head.setModified(_font.readDateTime());
		head.setXMin(_font.readShort());
		head.setYMin(_font.readShort());
		head.setXMax(_font.readShort());
		head.setYMax(_font.readShort());
		head.setMacStyle(_font.readUnsignedShort());
		head.setLowestRecPPEM(_font.readUnsignedShort());
		head.setFontDirectionHint(_font.readShort());
		head.setIndexToLocFormat(_font.readShort());
		head.setGlyphDataFormat(_font.readShort());
	}
	
	public void visitTableMAXP(TableMAXP maxp) throws IOException {
				
		maxp.setTableVersionNumber(_font.readFixed());
		maxp.setNumGlyphs(_font.readUnsignedShort());
		maxp.setMaxPoints(_font.readUnsignedShort());
		maxp.setMaxContours(_font.readUnsignedShort());
		maxp.setMaxCompositePoints(_font.readUnsignedShort());
		maxp.setMaxCompositeContours(_font.readUnsignedShort());
		maxp.setMaxZones(_font.readUnsignedShort());
		maxp.setMaxTwilightPoints(_font.readUnsignedShort());
		maxp.setMaxStorage(_font.readUnsignedShort());
		maxp.setMaxFunctionDefs(_font.readUnsignedShort());
		maxp.setMaxInstructionDefs(_font.readUnsignedShort());
		maxp.setMaxStackElements(_font.readUnsignedShort());
		maxp.setMaxSizeOfInstructions(_font.readUnsignedShort());
		maxp.setMaxComponentElements(_font.readUnsignedShort());
		maxp.setMaxComponentDepth(_font.readUnsignedShort());	
	}
	
	public void visitTableLOCA(TableLOCA loca) throws IOException {
				
		int    version = ((TableHEAD)FontManager.getInstance()
		        				.searchTable("head")).getIndexToLocFormat();
		int    n       = ((TableMAXP)FontManager.getInstance()
		        				.searchTable("maxp")).getNumGlyphs();
		long[] offsets = new long[n+1]; // extra offset op het einde -> lengte v/d glyf-table
		
		if (version == 0) {
			for (int i=0; i<n; i++) {
				offsets[i] = _font.readUnsignedShort()*2;
			}
		}
		else {
			for (int i=0; i<n; i++) {
				offsets[i] = _font.readUnsigned32();
			}
		}
		
		loca.setOffsets(offsets);
	}

	public void visitTableNAME(TableNAME name) throws IOException {
				
		long beginOfTable = _font.getFilePointer();
		
		name.setFormat(_font.readUnsignedShort());
		name.setCount(_font.readUnsignedShort());
		name.setStringOffset(_font.readUnsignedShort());
		
		int             count = name.getCount();
		TableNAMERecord nameRecord;
						
		/*
		 * NameRecords inlezen
		 */
		for (int i=0; i<count; i++) {
			nameRecord = new TableNAMERecord();
			
			nameRecord.setPlatformID(_font.readUnsignedShort());
			nameRecord.setEncodingID(_font.readUnsignedShort());
			nameRecord.setLanguageID(_font.readUnsignedShort());
			nameRecord.setNameID(_font.readUnsignedShort());
			nameRecord.setLength(_font.readUnsignedShort());
			nameRecord.setOffset(_font.readUnsignedShort());
			
			name.addNameRecord(nameRecord);
		}
		
		/*
		 * String data inlezen
		 * Data-offset = beginOfTable + beginOfStringData + offset_in_namerecord
		 */
		Vector   nameRecords       = name.getNameRecords();
		long     beginOfStringData = beginOfTable + name.getStringOffset();
		Iterator it                = nameRecords.iterator();
		byte[]   data;

		while (it.hasNext()) {	
			nameRecord = (TableNAMERecord)it.next();
			data       = new byte[nameRecord.getLength()];
			
			_font.seek(beginOfStringData + nameRecord.getOffset());	
			_font.read(data);
			
			nameRecord.setData(data);
		}
	}

	public void visitTableCMAP(TableCMAP cmap) throws IOException {
				
		/*
		 * Header inlezen
		 */
		long					beginOfTable = _font.getFilePointer();
		TableCMAPEncodingRecord record;
		
		cmap.setVersion(_font.readUnsignedShort());
		
		int n = _font.readUnsignedShort();
		cmap.setNumTables(n);
		
		/*
		 * encoding-records inlezen
		 */
		for (int i=0; i<n; i++) {
			record = new TableCMAPEncodingRecord();
			
			record.setPlatformID(_font.readUnsignedShort());
			record.setEncodingID(_font.readUnsignedShort());
			record.setOffset(_font.readUnsigned32());
			
			cmap.addCmapEncodingRecord(record);
		}
		
		/*
		 * visit iedere encoding-subtable
		 */
		Iterator          it = cmap.getCmapEncodingRecords().iterator();
		TableCMAPEncoding encoding;
		
		while(it.hasNext()) {
			record = (TableCMAPEncodingRecord)it.next();
			
			// de filepointer goedzetten voor de te lezen sub-table
			_font.seek(beginOfTable + record.getOffset());
			
			// bepaal table adhv format -> 
			// vul format in bij constructor (zie determineEncodingTable)
			int format = _font.readUnsignedShort();
			
			encoding = determineEncodingTable(format);
			encoding.accept(this);
			
			cmap.addCmapEncodingTable(encoding);		
		}	
	}
	
	public void visitTableCMAPFormat0(TableCMAPFormat0 format0) throws IOException {
			
		int[] glyphIdArray = new int[256];
		
		format0.setLength(_font.readUnsignedShort());
		format0.setLanguage(_font.readUnsignedShort());
		
		for (int i=0; i<glyphIdArray.length; i++) {
			glyphIdArray[i] = _font.readUnsignedByte();
		}
		
		format0.setGlyphIdArray(glyphIdArray);
		
	}
	
	// TODO Format2 mooi inlezen
	public void visitTableCMAPFormat2(TableCMAPFormat2 format2) throws IOException {
		
	    format2.setLength(_font.readUnsignedShort());
		format2.setLanguage(_font.readUnsignedShort());

		byte[] data = new byte[(int)format2.getLength()];
		_font.read(data);
		format2.setData(data);	
	}
	
	public void visitTableCMAPFormat4(TableCMAPFormat4 format4) throws IOException {
		
		long beginOfTable = _font.getFilePointer();
		
		format4.setLength(_font.readUnsignedShort());
		format4.setLanguage(_font.readUnsignedShort());
		
		int segCountX2 = _font.readUnsignedShort();
		format4.setSegCountX2(segCountX2);
		format4.setSearchRange(_font.readUnsignedShort());
		format4.setEntrySelector(_font.readUnsignedShort());
		format4.setRangeShift(_font.readUnsignedShort());
		
		int[]   endCount      = new int[segCountX2/2];
		int[]   startCount    = new int[segCountX2/2];
		short[] idDelta       = new short[segCountX2/2];
		int[]   idRangeOffset = new int[segCountX2/2];
		
		for (int i=0; i<endCount.length; i++) {
			endCount[i] = _font.readUnsignedShort();
		}
		
		format4.setEndCount(endCount);
		format4.setReservedPad(_font.readUnsignedShort());
		
		for (int i=0; i<startCount.length; i++) {
			startCount[i] = _font.readUnsignedShort();
		}
		
		for (int i=0; i<idDelta.length; i++) {
			idDelta[i] = _font.readShort();
		}
		
		for (int i=0; i<idRangeOffset.length; i++) {
			idRangeOffset[i] = _font.readUnsignedShort();
		}
		
		format4.setStartCount(startCount);
		format4.setIdDelta(idDelta);
		format4.setIdRangeOffset(idRangeOffset);
		
		// read glyphIdArray -> lengte = rest v/d table 
		int glyphIdArrayLength = ((int)format4.getLength() - 
		        					   (4 * segCountX2) - 16) / 2;
		int[] glyphIdArray = new int[glyphIdArrayLength];
		
		for (int i=0; i<glyphIdArray.length; i++) {
			glyphIdArray[i] = _font.readUnsignedShort();
		}
		
		format4.setGlyphIdArray(glyphIdArray);
	}
	
	public void visitTableCMAPFormat6(TableCMAPFormat6 format6) throws IOException {

		format6.setLength(_font.readUnsignedShort());
		format6.setLanguage(_font.readUnsignedShort());
		format6.setFirstCode(_font.readUnsignedShort());
		
		int entryCount     = _font.readUnsignedShort();
		int[] glyphIdArray = new int[entryCount];
		
		format6.setEntryCount(entryCount);
		
		for (int i=0; i<glyphIdArray.length; i++) {
			glyphIdArray[i] = _font.readUnsignedShort();
		}
		
		format6.setGlyphIdArray(glyphIdArray);		
	}
	
	public void visitTableCMAPFormat8(TableCMAPFormat8 format8) throws IOException {
		
		format8.setReserved(_font.readUnsignedShort());
		
		format8.setLength(_font.readUnsigned32());
		format8.setLanguage(_font.readUnsigned32());
		
		int[] is32 = new int[8192];
		
		for (int i=0; i<is32.length; i++) {
			is32[i] = _font.readUnsignedByte();
		}
		
		format8.setIs32(is32);
		
		long nGroups = _font.readUnsigned32();
		format8.setNGroups(nGroups);
		
		TableCMAPGroup group;
		
		for (long i=0; i<nGroups; i++) {
			group = new TableCMAPGroup();			
			group.accept(this);
			format8.addGroup(group);
		}	
	}
	
	public void visitTableCMAPFormat10(TableCMAPFormat10 format10) throws IOException {
		
		format10.setReserved(_font.readUnsignedShort());	
		format10.setLength(_font.readUnsigned32());
		format10.setLanguage(_font.readUnsigned32());
		format10.setStartCharCode(_font.readUnsigned32());
		
		long numChars = _font.readUnsigned32();
		format10.setNumChars(numChars);
		
		int[] glyphs = new int[(int)numChars];
		
		for (int i=0; i<numChars; i++) {
			glyphs[i] = _font.readUnsignedShort();
		}	
	}
	
	public void visitTableCMAPFormat12(TableCMAPFormat12 format12) throws IOException {
		
		format12.setReserved(_font.readUnsignedShort());
		format12.setLength(_font.readUnsigned32());
		format12.setLanguage(_font.readUnsigned32());
		
		long nGroups = _font.readUnsigned32();
		format12.setNGroups(nGroups);
		
		TableCMAPGroup group;
		
		for (long i=0; i<nGroups; i++) {
			group = new TableCMAPGroup();				
			group.accept(this);				
			format12.addGroup(group);
		}
	}
	
	public void visitTableCMAPGroup(TableCMAPGroup group) throws IOException {
		group.setStartCharCode(_font.readUnsigned32());
		group.setEndCharCode(_font.readUnsigned32());
		group.setStartGlyphID(_font.readUnsigned32());
	}
	
	public void visitTableGLYFSimple(TableGLYFSimple simpleGlyf) throws IOException {
		
	    /*
		 * endPointsOfContour is reeds gelezen en opgeslagen in simpleGlyf
		 * zie FontManager.determineGlyf()
		 */
		simpleGlyf.setXMin(_font.readShort());
		simpleGlyf.setYMin(_font.readShort());
		simpleGlyf.setXMax(_font.readShort());
		simpleGlyf.setYMax(_font.readShort());
		
		int[] endPointsOfContours = new int[simpleGlyf.getNumContours()];
		
		for (int i=0; i<endPointsOfContours.length; i++) {
			endPointsOfContours[i] = _font.readUnsignedShort();
		}
		
		simpleGlyf.setEndPtsOfContours(endPointsOfContours);
		
		int instructionLength = _font.readUnsignedShort();
		int[] instructions    = new int[instructionLength];
		
		for (int i=0; i<instructions.length; i++) {
			instructions[i] = _font.readUnsignedByte();
		}
		
		simpleGlyf.setInstructionLength(instructionLength);
		simpleGlyf.setInstructions(instructions);
		
		/*
		 * INLEZEN VAN DE FLAGS
		 * Ieder punt (coordinaten-paar) heeft een flag.  
		 * De flags in het bestand zijn gecomprimeerd en moeten gedecomprimeerd
		 * worden
		 * 
		 * 1. Een array voor ALLE flags wordt aangemaakt
		 * 2. Zolang deze array niet volledig is ingevuld, lezen we een flag uit 
		 *    het bestand
		 * 3. We kijken naar het 'repeat'-field en vullen we de flag x-keer in 
		 *    de array
		 * 
		 * numPoints        = laatste punt van laatste contour
		 * indexCurrentFlag = index in flag[] array
		 * flag             = flag in bestand
		 * repeatValue      = # keer repeat
		 * 
		 */
		int numPoints        = endPointsOfContours[endPointsOfContours.length-1]+1;
		int flags[]          = new int[numPoints];
		int indexCurrentFlag = 0;
		int flag;
		int repeatValue;

		while (indexCurrentFlag < numPoints) {
			
			flag = _font.readUnsignedByte();
			flags[indexCurrentFlag++] = flag;
			
			// de flag x keer herhalen
			if ((flag & TableGLYFSimple.REPEAT) != 0 ) {
				repeatValue = _font.readUnsignedByte();
												
				for (int i=0; i<repeatValue; i++) {
					flags[indexCurrentFlag++] = flag;
				}	
			}
		}	
		
		simpleGlyf.setFlags(flags);
		
		/*
		 * COORDINATES INLEZEN
		 * 
		 * Voor alle coordinaten gebruiken we de flags[] array ( zie boven )
		 * 
		 * SHORT_X             -->    X_SAME is teken ( 0 = negatief )
		 * !SHORT_X && X_SAME  -->    current coordinaat = previous coordinaat
		 * !SHORT_X && !X_SAME -->    coordinaat is een signed 16bit vector
		 *   
		 */	
		int[] xCoordinates = new int[numPoints];
		int[] yCoordinates = new int[numPoints];
		
		// X-coordinaten inlezen	
		for (int i=0; i<numPoints; i++) {
			
			if ( (flags[i] & TableGLYFSimple.BYTE_X) == 0) {
				if ( (flags[i] & TableGLYFSimple.SAME_X) == 0) {	
					xCoordinates[i] = _font.readShort(); // 16bit delta-vector
				}
			}
			else {											
				xCoordinates[i] = _font.readUnsignedByte();
				// teken bepalen: 0 is negatief
				if ( (flags[i] & TableGLYFSimple.SAME_X) == 0) {
					xCoordinates[i] = -xCoordinates[i];
				}
			}
		}
		
		// Y-coordinaten inlezen
		for (int i=0; i<numPoints; i++) {
			
			if ( (flags[i] & TableGLYFSimple.BYTE_Y) == 0) {
				if ( (flags[i] & TableGLYFSimple.SAME_Y) == 0) {	
					yCoordinates[i] = _font.readShort();
				}
			}
			else {											
				yCoordinates[i] = _font.readUnsignedByte();
				
				if ( (flags[i] & TableGLYFSimple.SAME_Y) == 0) {
					yCoordinates[i] = -yCoordinates[i];
				}	
			}
		}
		
		simpleGlyf.setXCoordinates(xCoordinates);
		simpleGlyf.setYCoordinates(yCoordinates);		
	}
	
	public void visitTableGLYFComposite(TableGLYFComposite compositeGlyf) throws IOException {
		
		/*
		 * endPointsOfContour is reeds gelezen en opgeslagen in simpleGlyf
		 * zie: FontManager.determineGlyf();
		 */ 
		compositeGlyf.setXMin(_font.readShort());
		compositeGlyf.setYMin(_font.readShort());
		compositeGlyf.setXMax(_font.readShort());
		compositeGlyf.setYMax(_font.readShort());
		
		GlyfComponent component;
		Vector        components = compositeGlyf.getComponents();
		int           flags;
		int           glyphIndex;
			
		/*
		 * Alle componenten v/d compositeglyph inlezen en toevoegen
		 */
		do {
			component = new GlyfComponent();
						
			flags = _font.readUnsignedShort();
			glyphIndex = _font.readUnsignedShort();
												
			component.setFlags(flags);
			component.setGlyphIndex(glyphIndex);	
			component.accept(this);
			components.add(component);			
			
		} while ((flags & TableGLYFComposite.MORE_COMPONENTS) != 0);
		
		/*
		 * Eventuele instructies inlezen
		 */
		if ((flags & TableGLYFComposite.WE_HAVE_INSTRUCTIONS) != 0) {
			int           numInstructions;
			int[]         instructions;
			
			numInstructions = _font.readUnsignedShort();
			instructions  = new int[numInstructions];
			
			for (int i=0; i<instructions.length; i++) {
				instructions[i] = _font.readUnsignedByte();
			}
			
			compositeGlyf.setNumInstructions(numInstructions);
			compositeGlyf.setInstructions(instructions);
		}		
	}
	
	public void visitTableGLYFCompositeComponent(GlyfComponent component) throws IOException {
		
		// flags opnieuw uit component uitlezen omdat we dat hier ook nodig hebben
		int flags = component.getFlags();
		
		if ((flags & TableGLYFComposite.ARG_1_AND_2_ARE_WORDS) != 0) {
			component.setArgument1(_font.readShort());
			component.setArgument2(_font.readShort());
		}
		else {
			component.setArgument1(_font.readUnsignedByte());
			component.setArgument2(_font.readUnsignedByte());
		}
		
		if ((flags & TableGLYFComposite.WE_HAVE_A_SCALE) != 0) {
			component.setScale(_font.readF2dot14());
		}
		else if ((flags & TableGLYFComposite.WE_HAVE_AN_X_AND_Y_SCALE) != 0) {
			component.setXScale(_font.readF2dot14());
			component.setYScale(_font.readF2dot14());			
		}
		else if ((flags & TableGLYFComposite.WE_HAVE_A_TWO_BY_TWO) != 0) {
			component.setXScale(_font.readF2dot14());
			component.setScale01(_font.readF2dot14());
			component.setScale10(_font.readF2dot14());
			component.setYScale(_font.readF2dot14());
		}		
	}
		
	public void visitTableOther(TableOther other) throws IOException {
		
		byte[] data = other.getData();
		_font.read(data);
	}
	
	private TableCMAPEncoding determineEncodingTable(int format) {
		
		TableCMAPEncoding table = null;
		
		switch(format) {
			case 0: table = new TableCMAPFormat0(); break;
			case 2: table = new TableCMAPFormat2(); break;
			case 4: table = new TableCMAPFormat4(); break;
			case 6: table = new TableCMAPFormat6(); break;
			case 8: table = new TableCMAPFormat8(); break;
			case 10: table = new TableCMAPFormat10(); break;
			case 12: table = new TableCMAPFormat12(); break;	
		}
		
		return table;
	}
}

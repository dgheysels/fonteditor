/*
 * Created on 20-feb-2005
 */
package datalayer;

import java.io.IOException;

/**
 * De interface voor de visitors (LoadTable/PrintTable/SaveTable)
 * 
 * @author Dimitri Gheysels
 */
abstract class TableVisitor {
	
	public abstract void visitTableOffset(TableOffset to) throws IOException;
	public abstract void visitTableDirectory(TableDirectory td) throws IOException;
	public abstract void visitTableHEAD(TableHEAD head) throws IOException;
	public abstract void visitTableMAXP(TableMAXP maxp) throws IOException;
	public abstract void visitTableLOCA(TableLOCA loca) throws IOException;
	public abstract void visitTableNAME(TableNAME name) throws IOException;
	public abstract void visitTableCMAP(TableCMAP cmap) throws IOException;
	public abstract void visitTableCMAPFormat0(TableCMAPFormat0 format0) throws IOException;
	public abstract void visitTableCMAPFormat2(TableCMAPFormat2 format2) throws IOException;
	public abstract void visitTableCMAPFormat4(TableCMAPFormat4 format4) throws IOException;
	public abstract void visitTableCMAPFormat6(TableCMAPFormat6 format6) throws IOException;
	public abstract void visitTableCMAPFormat8(TableCMAPFormat8 format8) throws IOException;
	public abstract void visitTableCMAPFormat10(TableCMAPFormat10 format10) throws IOException;
	public abstract void visitTableCMAPFormat12(TableCMAPFormat12 format12) throws IOException;
	public abstract void visitTableCMAPGroup(TableCMAPGroup group) throws IOException;
	public abstract void visitTableGLYFSimple(TableGLYFSimple simpleGlyf) throws IOException;
	public abstract void visitTableGLYFComposite(TableGLYFComposite compositeGlyf) throws IOException;
	public abstract void visitTableGLYFCompositeComponent(GlyfComponent component) throws IOException;
	public abstract void visitTableOther(TableOther other) throws IOException;	
}

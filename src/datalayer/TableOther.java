/*
 * Created on 24-feb-2005
 */
package datalayer;

import java.io.IOException;

class TableOther implements IVisitableTable {

	private byte[] _tag;
	private byte[] _data;
	
	public TableOther(String tag, long tableLength) {
		_data = new byte[(int)tableLength];
		_tag = tag.getBytes();
	}
	
	public byte[] getTag()  { return _tag; };
	public byte[] getData() { return _data; };
	
	public void setData(byte[] data) { _data = data; }
	
	public void accept(TableVisitor v) throws IOException {
		v.visitTableOther(this);
	}
}

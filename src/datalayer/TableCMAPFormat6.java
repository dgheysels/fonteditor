/*
 * Created on 23-feb-2005
 */
package datalayer;

import java.io.IOException;

class TableCMAPFormat6 extends TableCMAPEncoding {

	private int _firstCode;
	private int _entryCount;
	private int[] _glyphIdArray;
	
	public TableCMAPFormat6() {
		_format = 6;
	}
		
	public int getEntryCount() { return _entryCount; }
	public int getFirstCode() {	return _firstCode; }
	public int[] getGlyphIdArray() { return _glyphIdArray; }
	
	public void setEntryCount(int entryCount) { _entryCount = entryCount; }
	public void setFirstCode(int firstCode) { _firstCode = firstCode; }
	public void setGlyphIdArray(int[] glyphIdArray) { _glyphIdArray = glyphIdArray; }
	
	public void accept(TableVisitor v) throws IOException {
		v.visitTableCMAPFormat6(this);
	}
}

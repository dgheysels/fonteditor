/*
 * Created on 21-feb-2005
 */
package datalayer;

import java.io.IOException;
import java.util.*;

class TableNAME implements IVisitableTable {

	private int _format;
	private int _count;
	private int _stringOffset;
	private Vector _nameRecords = new Vector();
	
	public int getCount() { return _count; }
	public int getFormat() { return _format; }
	public Vector getNameRecords() { return _nameRecords; }
	public int getStringOffset() { return _stringOffset; }
	
	public void setCount(int count) { _count = count; }
	public void setFormat(int format) { _format = format; }
	public void setNameRecords(Vector nameRecords) { _nameRecords = nameRecords; }
	public void setStringOffset(int stringOffset) { _stringOffset = stringOffset; }
	
	public void addNameRecord(TableNAMERecord record) {
		_nameRecords.add(record);
	}
		
	public void accept(TableVisitor v) throws IOException {
		v.visitTableNAME(this);
	}
}

/*
 * Created on 23-feb-2005
 */
package datalayer;

import java.io.IOException;
import java.util.*;

class TableCMAPFormat8 extends TableCMAPEncoding {

	private int    _reserved;
	private int[]  _is32;
	private long   _nGroups;
	private Vector _groups = new Vector();
	
	public TableCMAPFormat8() {
		_format = 8;
	}
	
	public Vector getGroups() { return _groups; }
	public int[] getIs32() { return _is32; }
	public long getNGroups() { return _nGroups; }
	public int getReserved() { return _reserved; }
	
	public void setGroups(Vector groups) { _groups = groups; }
	public void setIs32(int[] is32) { _is32 = is32; }
	public void setNGroups(long nGroups) { _nGroups = nGroups; }
	public void setReserved(int reserved) { _reserved = reserved; }
	
	public void addGroup(TableCMAPGroup g) {
		_groups.add(g);
	}
	
	public void accept(TableVisitor v) throws IOException {
		v.visitTableCMAPFormat8(this);
	}
}

/*
 * Created on 23-feb-2005
 */
package datalayer;

import java.io.IOException;

class TableCMAPFormat10 extends TableCMAPEncoding {

	private int  _reserved;
	private long _startCharCode;
	private long _numChars;
	private int[] _glyphs;
	
	public TableCMAPFormat10() {
		_format = 10;
	}
	
	public int[] getGlyphs() { return _glyphs;	}
	public long getNumChars() { return _numChars; }
	public int getReserved() { return _reserved; }
	public long getStartCharCode() { return _startCharCode; }
		
	public void setGlyphs(int[] glyphs) { _glyphs = glyphs; }
	public void setNumChars(long numChars) { _numChars = numChars; }
	public void setReserved(int reserved) { _reserved = reserved; }
	public void setStartCharCode(long startCharCode) { _startCharCode = startCharCode; }
	
	public void accept(TableVisitor v) throws IOException {
		v.visitTableCMAPFormat10(this);	
	}
}

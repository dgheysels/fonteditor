/*
 * Created on 22-feb-2005
 */
package datalayer;

import java.io.IOException;

class TableCMAPFormat0 extends TableCMAPEncoding {
	
	private int[] _glyphIdArray;
	
	public TableCMAPFormat0() {
		_format = 0;
	}
	
	public int[] getGlyphIdArray() { return _glyphIdArray; }
	public void setGlyphIdArray(int[] glyphIdArray) { _glyphIdArray = glyphIdArray; }
	
	public void accept(TableVisitor v) throws IOException {
		v.visitTableCMAPFormat0(this);
	}
}


/*
 * Created on 19-mei-2005
 */
package datalayer;

/**
 * @author Dimitri Gheysels
 */
interface IDatalayerConstants {

   int BEGINTABLEDIR = 12;
   int LENGTHTABLEDIRENTRY = 16;
   
   int UNICODE_PLATFORM = 0;
   int MICROSOFT_PLATFORM = 3;
   
   int UNICODE11_ENCODING = 1;
   
    
}

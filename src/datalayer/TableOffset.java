/*
 * Created on 20-feb-2005
 */
package datalayer;

import java.io.IOException;
import datatypes.*;

class TableOffset implements IVisitableTable {
	
	private Fixed  _sfnt;
	private int    _numTables;
	private int    _searchRange;
	private int    _entrySelector;
	private int    _rangeShift;
	
	public Fixed getSfnt() { return _sfnt; }
	public int  getNumTables() { return _numTables; }
	public int  getSearchRange() { return _searchRange; }
	public int  getEntrySelector() { return _entrySelector; }
	public int  getRangeShift() { return _rangeShift; }
	
	public void setSfnt(Fixed sfnt) { _sfnt = sfnt; }
	public void setNumTables(int numTables) { _numTables = numTables; }
	public void setSearchRange(int searchRange) { _searchRange = searchRange; }
	public void setEntrySelector(int entrySelector) { _entrySelector = entrySelector; }
	public void setRangeShift(int rangeShift) { _rangeShift = rangeShift; }
	
	public void accept(TableVisitor v) throws IOException {
		v.visitTableOffset(this);
	}
}

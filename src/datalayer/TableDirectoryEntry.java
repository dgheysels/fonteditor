/*
 * Created on 20-feb-2005
 */
package datalayer;

class TableDirectoryEntry {
	private byte[] _tag;
	private long   _offset;
	private long   _length;
	private long   _checksum;
	
	public TableDirectoryEntry() {}
	
	public byte[] getTag() { return _tag; }	
	public long   getOffset() {return _offset; }
	public long   getLength() {return _length; }
	public long   getChecksum() { return _checksum; }
	
	public void   setTag(byte[] tag) { _tag = tag; }
	public void   setChecksum(long checksum) { _checksum = checksum; }
	public void   setOffset(long offset) { _offset = offset; }
	public void   setLength(long length) { _length = length; }
	
	public String toString() {
		return new String(_tag) + " " + _offset + " " + _length + " " + Long.toHexString(_checksum);
	}
}

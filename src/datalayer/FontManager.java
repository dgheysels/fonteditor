/*
 * Created on 20-feb-2005
*/

package datalayer;

import model.*;
import extra.*;

import java.io.*;
import java.util.*;

/**
 * Deze Singleton-klasse is de interface voor de Datalaag.  
 * Alle tables en glyphs worden hier bijgehouden in collections
 * 
 * @author Dimitri Gheysels
 */
public class FontManager implements IDatalayerConstants {
	
	private static FontManager 				_instance = null;
	private        String   				_fontname;
	private        ExtendedRandomAccessFile _font;
	private		   SortedMap<String, IVisitableTable> _tables;
	private        Vector    				_glyphs;
			
	private FontManager() {
		_tables = new TreeMap<String, IVisitableTable>();
		_glyphs = new Vector();
	}
	
	/**
	 * getInstance() heeft een instantie van deze klasse terug
	 * 
	 * @return a FontManager
	 */
	public static FontManager getInstance() {
		if (_instance == null) {
			_instance = new FontManager();
		}
		
		return _instance;
	}
	
	/**
	 * Deze methode geeft interessante informatie terug over de font.  (fontnaam, versie, ...)
	 * 
	 * @return een FontInformation object
	 * @throws UnsupportedEncodingException
	 */
	public FontInformation getFontInformation() throws UnsupportedEncodingException {
		FontInformation info = new FontInformation();
		
		TableHEAD       head        = (TableHEAD)searchTable("head");
		TableMAXP       maxp        = (TableMAXP)searchTable("maxp");
		Vector          nameRecords = ((TableNAME)searchTable("name")).getNameRecords();
		TableNAMERecord record;
		
		/* HEAD info */
		info.setUnitsPerEM(head.getUnitsPerEm());
		info.setCreated(head.getCreated().toDate());
		info.setModified(head.getModified().toDate());
		info.setXMin(head.getXMin());
		info.setYMin(head.getYMin());
		info.setXMax(head.getXMax());
		info.setYMax(head.getYMax());
		
		/* MAXP info */
		info.setNumberOfGlyphs(maxp.getNumGlyphs());
		
		/* NAME info */
		Iterator it = nameRecords.iterator();
		
		while (it.hasNext()) {		
			record = (TableNAMERecord)it.next();
			
			// UNICODE platform of MICROSOFTplatform met UNICODE encodering
			if (record.getPlatformID() == UNICODE_PLATFORM || 
			    (record.getPlatformID() == MICROSOFT_PLATFORM && record.getEncodingID() == UNICODE11_ENCODING)) {
				
				switch(record.getNameID()) {
					case 0: info.setCopyright(Convert.byteToString16BE(record.getData())); break;
					case 1: info.setFontFamily(Convert.byteToString16BE(record.getData())); break;				
					case 4: info.setFontName(Convert.byteToString16BE(record.getData())); break;
					case 5: info.setFontVersion(Convert.byteToString16BE(record.getData())); break;
					case 7: info.setTrademark(Convert.byteToString16BE(record.getData())); break;
					case 8: info.setManufacturerName(Convert.byteToString16BE(record.getData())); break;
					case 9: info.setDesignerName(Convert.byteToString16BE(record.getData())); break;
					case 10: info.setTypefaceDescription(Convert.byteToString16BE(record.getData())); break;
					case 11: info.setVendorURL(Convert.byteToString16BE(record.getData())); break;
					case 12: info.setDesignerURL(Convert.byteToString16BE(record.getData())); break;
					case 13: info.setLicenceDescription(Convert.byteToString16BE(record.getData())); break;
					case 14: info.setLicenceURL(Convert.byteToString16BE(record.getData())); break;
				}
			}	
		}
		
		return info;
	}
	
	/**
	 * Not Yet Implemented
	 */
	public Glyph getGlyph(char charcode) {
		return null;
	}
	
	/**
	 * Deze methode creeert een conceptuele Glyph.
	 * 
	 * @param glyphID ID van de glyph
	 * @return Glyph
	 */
	public Glyph getGlyph(int glyphID) {
				
		TableGLYF g = (TableGLYF)_glyphs.get(glyphID);	
		
		if (g != null) {
		    Glyph glyph = g.getAsConceptualGlyph();
		    glyph.setID(glyphID);
				
		    return glyph;
		}
		else {
		    return null;
		}
	}
	
	/**
	 * Geeft alle glyphs terug in het font als een conceptuele Glyph
	 * 
	 * @return Glyph array
	 */
	public Glyph[] getAllGlyphs() {
		
		Glyph[] glyphs = new Glyph[((TableMAXP)searchTable("maxp")).getNumGlyphs()];
		
		for (int i=0; i<glyphs.length; i++) {
			Counter.reset();
			glyphs[i] = getGlyph(i);
		}
		
		return glyphs;		
	}
	
	/**
	 * Een conceptuele glyph terug opslaan in de datalaag.  
	 * 
	 * @param g A conceptual Glyph
	 */
	public void setGlyph(Glyph g) {
		
	    TableGLYF tableGlyf = (TableGLYF)_glyphs.get(g.getID());
	    tableGlyf.setFromConceptualGlyph(g);
	}
	
	/**
	 * Deze methode laadt een font in de Datalaag.  De in te laden font wordt bepaald door 
	 * de fontnaam, welke wordt gezet mbv de setFontname() method.
	 * 
	 * @see FontManager#setFontname(String)
	 * @throws IOException
	 */
	public void loadFont() throws IOException {
		
		_font = new ExtendedRandomAccessFile(_fontname, "r");
		
		LoadTableVisitor loadVisitor = new LoadTableVisitor(_font);		
		
		TableOffset tableOffset = new TableOffset();
		_font.seek(0);
		tableOffset.accept(loadVisitor);
		_tables.put("OFFSET", tableOffset);
		
		TableDirectory tableDirectory = new TableDirectory();
		tableDirectory.accept(loadVisitor);
		_tables.put("TABLEDIRECTORY", tableDirectory);
		
		/*
		 * INLEZEN V/D TABLES
		 * 1. Een nieuwe table maken
		 * 2. De filepointer goed zetten.
		 * 3. De loadVisitor zal de table inlezen
		 */				
		TableHEAD tableHead = new TableHEAD();
		_font.seek(tableDirectory.findEntryByTag("head").getOffset());
		tableHead.accept(loadVisitor);
		_tables.put("head", tableHead);
		
		TableMAXP tableMaxp = new TableMAXP();
		_font.seek(tableDirectory.findEntryByTag("maxp").getOffset());
		tableMaxp.accept(loadVisitor);
		_tables.put("maxp", tableMaxp);
		
		TableLOCA tableLoca = new TableLOCA();
		_font.seek(tableDirectory.findEntryByTag("loca").getOffset());
		tableLoca.accept(loadVisitor);
		_tables.put("loca", tableLoca);
		
		TableNAME tableName = new TableNAME();
		_font.seek(tableDirectory.findEntryByTag("name").getOffset());
		tableName.accept(loadVisitor);
		_tables.put("name", tableName);	
		
		TableCMAP tableCmap = new TableCMAP();
		_font.seek(tableDirectory.findEntryByTag("cmap").getOffset());
		tableCmap.accept(loadVisitor);
		_tables.put("cmap", tableCmap);
	
		/*
		 * GLYPHS INLEZEN
		 * TableLOCA bevat de offsets van de glyphs.  
		 * Wanneer een offset = vorige offset, dan heeft de betreffende glyph geen contouren 
		 * -> null-value in _glyphs
		 */		
		TableGLYF glyph;
		int       numGlyphs         = tableMaxp.getNumGlyphs();
		long      holdGlyfOffset    = -1;
		long	  currentGlyfOffset = 0; 
		long      startOfGlyfTable  = tableDirectory.findEntryByTag("glyf").getOffset();
		
		for (int i=0; i<numGlyphs; i++) {
						
		    currentGlyfOffset = tableLoca.getOffset(i);
		    
		    // glyph zonder contour = null-glyph
			if (currentGlyfOffset == holdGlyfOffset) {				
				_glyphs.add(i, null);
			}
			else {
				_font.seek(startOfGlyfTable + currentGlyfOffset);
				
				glyph = makeGlyf();
				glyph.accept(loadVisitor);
				_glyphs.add(i, glyph);				
				
				holdGlyfOffset = currentGlyfOffset;
			}
		}	
			
		/*
		 * OTHER TABLES INLEZEN
		 * 1. Overloop de tableDirectory
		 * 2. Voor de tables die <> van cmap/glyp/name/loca/maxp/offset een OtherTable object maken
		 * 3. Filepointer goed zetten en accepten
		 * 4. De table toevoegen aan de _tables-vector
		 */	
		Iterator     		 it = tableDirectory.getTableDirectoryEntries().entrySet().iterator();
		TableDirectoryEntry  current;
		String	      		 tag;
		TableOther    		 other;
		
		while (it.hasNext()) {
			current = (TableDirectoryEntry)((Map.Entry)it.next()).getValue();
			tag     = new String(current.getTag());

			if (!tag.equalsIgnoreCase("head") && !tag.equalsIgnoreCase("maxp") &&
				!tag.equalsIgnoreCase("loca") && !tag.equalsIgnoreCase("name") && 
				!tag.equalsIgnoreCase("cmap") && !tag.equalsIgnoreCase("glyf")) {
			
				other = new TableOther(tag, current.getLength());
				_font.seek(current.getOffset());
				other.accept(loadVisitor);
				
				_tables.put(tag, other);				
			}	
		}	
		
		printFont(new PrintStream("FONT.txt"));
		
		_font.close();	
	}
	
	/**
	 * printFont() zal de inhoud van de Datalaag naar een PrintStream object schrijven
	 * bv: een ASCII-file of de standaard output.
	 * 
	 * @throws IOException
	 */
	public void printFont(PrintStream output) throws IOException {
		
		PrintTableVisitor printVisitor = new PrintTableVisitor(output);
		
		output.println(Calendar.getInstance().getTime().toString());
		output.println();
		output.println("TABLES");
		output.println("++++++++++++++++++++++++++++++++++++++++++");
		
		Iterator it = _tables.entrySet().iterator();
		
		while(it.hasNext()) {
			((IVisitableTable)
					((Map.Entry)it.next()).getValue())
							  .accept(printVisitor);
		}
		
		output.println();		
		output.println("GLYPHS");
		output.println("+++++++++++++++++++++++++++++++++++++++++");
		
		Iterator  i = _glyphs.iterator();
		TableGLYF current;
		while (i.hasNext()) {
			current = (TableGLYF)i.next();
			
			if (current != null)
				current.accept(printVisitor);
			else
				output.println(printVisitor._glyphCounter++ + "NULL glyph");
		}
		
		output.close();
	}
	
	/**
	 * De Datalaag terug opslaan.  De naam van het bestand wordt opnieuw bepaald door de 
	 * fontnaam.  De fontnaam kan mbv setFontname() gezet worden.
	 * 
	 * @throws IOException
	 */
	public void saveFont() throws IOException {
		
	    _font = new ExtendedRandomAccessFile(_fontname, "rw");
		
		TableDirectory   newTableDirectory = new TableDirectory();
		SortedMap        entries           = newTableDirectory.getTableDirectoryEntries();
		SaveTableVisitor saveVisitor       = new SaveTableVisitor(_font, entries);
		
		TableOffset offset = ((TableOffset)_tables.get("OFFSET"));
		
		offset.accept(saveVisitor);
		
		/*
		 * Maak genoeg plaats voor de TableDirectory.  De TableDirectory wordt achteraf geschreven
		 * #tables * 16byte/entry
		 */
		byte[] tabledir_placeholder = new byte[_tables.size() * LENGTHTABLEDIRENTRY];
		_font.write(tabledir_placeholder);
		
		/*
		 * WEGSCHRIJVEN VAN DE TABLES
		 * Schrijf alle tables weg (behalve Offset, TableDirectory en Loca).  De tables halen we
		 * uit de _tables collection.
		 * Tijdens het schrijven van de tables wordt er een nieuwe tabledirectory geconstrueerd.  
		 */
		Iterator            it = _tables.entrySet().iterator();
		String              tag;
		IVisitableTable     currentTable;
		           
		while(it.hasNext()) {	
			Map.Entry temp = (Map.Entry)it.next();
						
			tag = temp.getKey().toString();
			currentTable = (IVisitableTable)temp.getValue();
			
			if (!tag.equals("OFFSET") && !tag.equals("TABLEDIRECTORY") && !tag.equals("loca")) {	
				currentTable.accept(saveVisitor);
			}		
		}
		
		/*
		 * WEGSCHRIJVEN VAN DE GLYPHS > GLYF-table
		 * Alle glyfs bevinden zich in de _glyphs collection.  Voor iedere glyph dat er wordt weggeschreven
		 * wordt ook de offset in de 'loca'-table aangepast.  De 'loca'-table wordt helemaal op het einde
		 * van het bestand weggeschreven.
		 */
		TableDirectoryEntry glyfEntry  = new TableDirectoryEntry();
		TableLOCA           locatable  = (TableLOCA)_tables.get("loca");
		TableGLYF           glyph;
		long				beginGlyfTable;
		int                 locaoffset = 0;
		int                 i          = 0;
		
		// GLYF-entry voor in de tabledirectory aanmaken
		glyfEntry.setTag(new String("glyf").getBytes());
		beginGlyfTable = _font.getFilePointer();
		glyfEntry.setOffset(beginGlyfTable);
		
		// alle glyphs wegschrijven
		it = _glyphs.iterator();

		while (it.hasNext()) {	
		   	glyph = (TableGLYF)it.next();
			
			// offset naar glyph berekenen (relatief met begin v/d GlyfTable)
			// NULL-glyph = glyph zonder contour
		   	if (glyph != null) {
				locaoffset = (int)(_font.getFilePointer() - beginGlyfTable);
				locatable.setOffset(i, locaoffset);
				glyph.accept(saveVisitor);
			}
			else { 
				// NULL-glyph heeft dezelfde offset als de vorige.  
				// Offset overnemen van de vorige iteratie
				locatable.setOffset(i, locaoffset);	
			}
		   	
			i++;
		}
		
		// extra offset in LOCA om de lengte v/d laatste glyph te weten
		// offset = einde v/d table
		locatable.setOffset(i, _font.getFilePointer() - beginGlyfTable);
		
		// glyfEntry updaten en toevoegen a/d tableDirectory-entries
		glyfEntry.setLength(_font.getFilePointer() - beginGlyfTable);
		glyfEntry.setChecksum(
				((TableDirectory)FontManager.getInstance().searchTable("TABLEDIRECTORY")).
				findEntryByTag("glyf").getChecksum()
			);  // TODO: checksum berekenen
		
		entries.put("glyf", glyfEntry);
	
		// locaTable wegschrijven
		locatable.accept(saveVisitor);					
				
		/*
		 * TABLEDIRECTORY wegschrijven
		 */
		_font.seek(BEGINTABLEDIR);	
		newTableDirectory.accept(saveVisitor);	
		
		_font.close();			
	}
	
	public void close() throws IOException {
		_font = null;
		_tables = null;
		_glyphs = null;
		_instance = null;
	}
	
	/**
	 * Een fonttable opzoeken in de Datalaag
	 * 
	 * @param name Tag of the table
	 * @return the table as a IVisitableTable
	 */
	public IVisitableTable searchTable(String name) {
		return _tables.get(name);
	}
	
	/**
	 * De bestandsnaam van de huidige font teruggeven
	 * 
	 * @return name of the font
	 */
	public String getFontname() { 
		return _fontname; 
	}
	
	/**
	 * De bestandsnaam voor de huidige font zetten
	 * 
	 * @param name of the font
	 */
	public void setFontname(String name) { 
		_fontname = name; 
	}
	
	
	private TableGLYF makeGlyf() throws IOException {
		
		short numContours = _font.readShort();
				
		if (numContours > 0 ) {
			return new TableGLYFSimple(numContours);
		}
		else {
			return new TableGLYFComposite(numContours);
		}
	}
}

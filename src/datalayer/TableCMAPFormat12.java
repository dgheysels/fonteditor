/*
 * Created on 23-feb-2005
 */
package datalayer;

import java.io.IOException;
import java.util.*;

class TableCMAPFormat12 extends TableCMAPEncoding {

	private int    _reserved;
	private long   _nGroups;
	private Vector _groups = new Vector();
	
	public TableCMAPFormat12() {
		_format = 12;
	}
	
	public Vector getGroups() { return _groups; }
	public long getNGroups() { return _nGroups; }
	public int getReserved() { return _reserved; }
	
	public void setGroups(Vector groups) { _groups = groups; }
	public void setNGroups(long nGroups) { _nGroups = nGroups; }
	public void setReserved(int reserved) { _reserved = reserved; }
	
	public void addGroup(TableCMAPGroup g) {
		_groups.add(g);
	}
	
	public void accept(TableVisitor v) throws IOException {
		v.visitTableCMAPFormat12(this);
	}
}

/*
 * Created on 22-feb-2005
 */
package datalayer;

import java.io.IOException;
import java.util.*;

class TableCMAPFormat2 extends TableCMAPEncoding {
	
	/* TEMP */
	byte[] _data;
	/* /TEMP */
	
	private int[] _subHeaderKeys;
	private int[] _glyphIndexArray;
	private Vector _subHeaders = new Vector();
		
	public TableCMAPFormat2() {
		_format = 2;
	}
	
	/* TEMP */
	public byte[] getData() { return _data; }
	public void setData(byte[] data) { _data = data; }
	/* /TEMP */
	
	public int[] getGlyphIndexArray() { return _glyphIndexArray; }
	public int[] getSubHeaderKeys() { return _subHeaderKeys; }
	public Vector getSubHeaders() { return _subHeaders; }
	
	public void setGlyphIndexArray(int[] glyphIndexArray) {	_glyphIndexArray = glyphIndexArray; }
	public void setSubHeaderKeys(int[] subHeaderKeys) { _subHeaderKeys = subHeaderKeys; }
	public void setSubHeaders(Vector subHeaders) { _subHeaders = subHeaders; }
	
	public void accept(TableVisitor v) throws IOException {
		v.visitTableCMAPFormat2(this);
	}
}

/*
 * Created on 20-feb-2005
 */
package datalayer;

import java.io.IOException;

import datatypes.*;

class TableHEAD implements IVisitableTable {

	private Fixed  _tableVersionNumber;
	private Fixed  _fontRevision;
	private long   _checksumAdjustment;
	private long   _magicNumber;
	private int    _flags;
	private int    _unitsPerEm;
	private DateTime _created;
	private DateTime _modified;
	private short  _xMin;
	private short  _yMin;
	private short  _xMax;
	private short  _yMax;
	private int    _macStyle;
	private int    _lowestRecPPEM;
	private short  _fontDirectionHint;
	private short  _indexToLocFormat;
	private short  _glyphDataFormat;
	
	public long getChecksumAdjustment() { return _checksumAdjustment; }
	public DateTime getCreated() { return _created; }
	public int getFlags() { return _flags; }
	public short getFontDirectionHint() { return _fontDirectionHint; }
	public Fixed getFontRevision() { return _fontRevision; }
	public short getGlyphDataFormat() { return _glyphDataFormat; }
	public short getIndexToLocFormat() { return _indexToLocFormat; }
	public int getLowestRecPPEM() { return _lowestRecPPEM; }
	public int getMacStyle() {	return _macStyle; }
	public long getMagicNumber() {	return _magicNumber; }
	public DateTime getModified() { return _modified; }
	public Fixed getTableVersionNumber() { return _tableVersionNumber; }
	public int getUnitsPerEm() { return _unitsPerEm; }
	public short getXMax() { return _xMax; }
	public short getXMin() { return _xMin; }
	public short getYMax() { return _yMax;	}
	public short getYMin() { return _yMin;	}
	
	public void setChecksumAdjustment(long adjustment) { _checksumAdjustment = adjustment; }
	public void setCreated(DateTime created) { _created = created; }
	public void setFlags(int flags) { _flags = flags; }
	public void setFontDirectionHint(short fontDirectionHint) { _fontDirectionHint = fontDirectionHint; }
	public void setFontRevision(Fixed fontRevision) {	_fontRevision = fontRevision; }
	public void setGlyphDataFormat(short glyphDataFormat) { _glyphDataFormat = glyphDataFormat; }
	public void setIndexToLocFormat(short indexToLocFormat) { _indexToLocFormat = indexToLocFormat; }
	public void setLowestRecPPEM(int lowestRecPPEM) { _lowestRecPPEM = lowestRecPPEM; }
	public void setMacStyle(int macStyle) { _macStyle = macStyle; }
	public void setMagicNumber(long magicNumber) {	_magicNumber = magicNumber; }
	public void setModified(DateTime modified) { _modified = modified; }
	public void setTableVersionNumber(Fixed tableVersionNumber) { _tableVersionNumber = tableVersionNumber; }
	public void setUnitsPerEm(int unitsPerEm) { _unitsPerEm = unitsPerEm; }
	public void setXMax(short xMax) { _xMax = xMax; }
	public void setXMin(short xMin) { _xMin = xMin; }
	public void setYMax(short yMax) { _yMax = yMax; }
	public void setYMin(short yMin) { _yMin = yMin; }
	
	public void accept(TableVisitor v) throws IOException {
		v.visitTableHEAD(this);	
	}
}

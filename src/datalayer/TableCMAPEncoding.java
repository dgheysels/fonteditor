/*
 * Created on 22-feb-2005
 */
package datalayer;

import java.io.IOException;

abstract class TableCMAPEncoding implements IVisitableTable {

	protected int  _format;
	protected long _length;
	protected long _language;
	
	public int getFormat() { return _format; }
	public long getLanguage() { return _language; }
	public long getLength() { return _length; }
	
	public void setFormat(int format) { _format = format; }
	public void setLanguage(long language) { _language = language; }
	public void setLength(long length) { _length = length; }
	
	public abstract void accept(TableVisitor v) throws IOException;
}

/*
 * Created on 20-feb-2005
 */
package datalayer;

import java.io.IOException;
import java.util.*;

class TableDirectory implements IVisitableTable {
	
	private SortedMap _entries = new TreeMap();
		
	public SortedMap getTableDirectoryEntries() { return _entries; }
		
	public void addDirectoryEntry(String key, TableDirectoryEntry entry) {
		_entries.put(key, entry);
	}
	
	public TableDirectoryEntry findEntryByTag(String tag) {
		return (TableDirectoryEntry)_entries.get(tag);
	}
	
	public void accept(TableVisitor v) throws IOException {
			v.visitTableDirectory(this);
	}
}

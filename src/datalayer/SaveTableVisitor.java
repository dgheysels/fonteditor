/*
 * Created on 7-mrt-2005
 */
package datalayer;

import java.io.IOException;
import java.util.*;

class SaveTableVisitor extends TableVisitor {

    private ExtendedRandomAccessFile _font;

    private SortedMap _entries;

    private TableDirectoryEntry _newEntry;

    public SaveTableVisitor(ExtendedRandomAccessFile f, SortedMap entries)
            throws IOException {
        _font = f;
        // tabledirectoryentries moeten bijgewerkt worden. SaveVisitor krijgt
        // een referentie mee van de entries (SortedMap) v/d tabledirectory uit
        // de FontManager
        _entries = entries;
    }

    public void visitTableOffset(TableOffset to) throws IOException {
        _font.writeFixed(to.getSfnt());
        _font.writeShort(to.getNumTables());
        _font.writeShort(to.getSearchRange());
        _font.writeShort(to.getEntrySelector());
        _font.writeShort(to.getRangeShift());
    }

    public void visitTableDirectory(TableDirectory td) throws IOException {

        SortedMap entries = td.getTableDirectoryEntries();
        Iterator it = entries.entrySet().iterator();

        TableDirectoryEntry currentEntry;

        while (it.hasNext()) {
            currentEntry = (TableDirectoryEntry) ((Map.Entry) it.next())
                    .getValue();

            _font.write(currentEntry.getTag());
            _font.writeUnsigned32(currentEntry.getChecksum());
            _font.writeUnsigned32(currentEntry.getOffset());
            _font.writeUnsigned32(currentEntry.getLength());
        }
    }

    public void visitTableHEAD(TableHEAD head) throws IOException {

        /*
         * nieuwe tableDirectoryEntry aanmaken
         */
        String tag = "head";
        _newEntry = new TableDirectoryEntry();

        _newEntry.setTag(tag.getBytes());
        _newEntry.setOffset(_font.getFilePointer());

        /*
         * data wegschrijven
         */
        _font.writeFixed(head.getTableVersionNumber());
        _font.writeFixed(head.getFontRevision());
        _font.writeInt((int) head.getChecksumAdjustment());
        _font.writeInt((int) head.getMagicNumber());
        _font.writeShort(head.getFlags());
        _font.writeShort(head.getUnitsPerEm());
        _font.writeDateTime(head.getCreated());
        _font.writeDateTime(head.getModified());
        _font.writeShort(head.getXMin());
        _font.writeShort(head.getYMin());
        _font.writeShort(head.getXMax());
        _font.writeShort(head.getYMax());
        _font.writeShort(head.getMacStyle());
        _font.writeShort(head.getLowestRecPPEM());
        _font.writeShort(head.getFontDirectionHint());
        _font.writeShort(head.getIndexToLocFormat());
        _font.writeShort(head.getGlyphDataFormat());

        /*
         * nieuwe tableDirectoryEntry updaten en toevoegen aan de
         * tabledirectory-entries
         */
        _newEntry.setLength(_font.getFilePointer() - _newEntry.getOffset());

        _newEntry.setChecksum(((TableDirectory)FontManager.getInstance()
		                .searchTable("TABLEDIRECTORY")).findEntryByTag("head")
		                .getChecksum()); // TODO: checksum berekenen

        _entries.put(tag, _newEntry);

        /*
         * table padden totdat de lengte een veelvoud is van 4
         */
        padTable();
    }

    public void visitTableMAXP(TableMAXP maxp) throws IOException {

        String tag = "maxp";
        _newEntry = new TableDirectoryEntry();

        _newEntry.setTag(tag.getBytes());
        _newEntry.setOffset(_font.getFilePointer());

        _font.writeFixed(maxp.getTableVersionNumber());
        _font.writeShort(maxp.getNumGlyphs());
        _font.writeShort(maxp.getMaxPoints());
        _font.writeShort(maxp.getMaxContours());
        _font.writeShort(maxp.getMaxCompositePoints());
        _font.writeShort(maxp.getMaxCompositeContours());
        _font.writeShort(maxp.getMaxZones());
        _font.writeShort(maxp.getMaxTwilightPoints());
        _font.writeShort(maxp.getMaxStorage());
        _font.writeShort(maxp.getMaxFunctionDefs());
        _font.writeShort(maxp.getMaxInstructionDefs());
        _font.writeShort(maxp.getMaxStackElements());
        _font.writeShort(maxp.getMaxSizeOfInstructions());
        _font.writeShort(maxp.getMaxComponentElements());
        _font.writeShort(maxp.getMaxComponentDepth());

        _newEntry.setLength(_font.getFilePointer() - _newEntry.getOffset());

        _newEntry.setChecksum(((TableDirectory)FontManager.getInstance()
                		.searchTable("TABLEDIRECTORY")).findEntryByTag("maxp")
                		.getChecksum()); // TODO: checksum berekenen

        _entries.put(tag, _newEntry);

        padTable();
    }

    public void visitTableLOCA(TableLOCA loca) throws IOException {

        String tag = "loca";
        _newEntry = new TableDirectoryEntry();

        _newEntry.setTag(tag.getBytes());
        _newEntry.setOffset(_font.getFilePointer());

        int version = ((TableHEAD) FontManager.getInstance()
                .searchTable("head")).getIndexToLocFormat();
        long[] offsets = loca.getOffsets();

        if (version == 0) {
            for (int i = 0; i < offsets.length; i++) {
                _font.writeShort((int) offsets[i] / 2);
            }
        } else {
            for (int i = 0; i < offsets.length; i++) {
                _font.writeInt((int) offsets[i]);
            }
        }

        _newEntry.setLength(_font.getFilePointer() - _newEntry.getOffset());
        _newEntry.setChecksum(((TableDirectory) FontManager.getInstance()
		                .searchTable("TABLEDIRECTORY")).findEntryByTag("loca")
		                .getChecksum()); // TODO: checksum berekenen

        _entries.put(tag, _newEntry);

        padTable();
    }

    public void visitTableNAME(TableNAME name) throws IOException {

        String tag = "name";
        _newEntry = new TableDirectoryEntry();

        _newEntry.setTag(tag.getBytes());
        _newEntry.setOffset(_font.getFilePointer());

        _font.writeShort(name.getFormat());
        _font.writeShort(name.getCount());
        _font.writeShort(name.getStringOffset());

        //alle namerecords wegschrijven
        Vector nameRecords = name.getNameRecords();
        Iterator it = nameRecords.iterator();

        TableNAMERecord current;

        while (it.hasNext()) {
            current = (TableNAMERecord) it.next();

            _font.writeShort(current.getPlatformID());
            _font.writeShort(current.getEncodingID());
            _font.writeShort(current.getLanguageID());
            _font.writeShort(current.getNameID());
            _font.writeShort(current.getLength());
            _font.writeShort(current.getOffset());
        }

        // alle string data wegschrijven
        it = nameRecords.iterator();

        while (it.hasNext()) {
            current = (TableNAMERecord) it.next();
            _font.write(current.getData());
        }

        _newEntry.setLength(_font.getFilePointer() - _newEntry.getOffset());
        _newEntry.setChecksum(((TableDirectory) FontManager.getInstance()
		                .searchTable("TABLEDIRECTORY")).findEntryByTag("name")
		                .getChecksum()); // TODO: checksum berekenen

        _entries.put(tag, _newEntry);

        padTable();
    }

    public void visitTableCMAP(TableCMAP cmap) throws IOException {

        /*
         * nieuwe tableDirectoryEntry aanmaken
         */
        String tag = "cmap";
        _newEntry = new TableDirectoryEntry();

        _newEntry.setTag(tag.getBytes());
        _newEntry.setOffset(_font.getFilePointer());

        /*
         * data wegschrijven
         */
        _font.writeShort(cmap.getVersion());
        _font.writeShort(cmap.getNumTables());

        // Alle encoding-records wegschrijven.
        Vector encodingRecords = cmap.getCmapEncodingRecords();
        Iterator it = encodingRecords.iterator();
        TableCMAPEncodingRecord currentRecord;

        while (it.hasNext()) {

            currentRecord = (TableCMAPEncodingRecord) it.next();

            _font.writeShort(currentRecord.getPlatformID());
            _font.writeShort(currentRecord.getEncodingID());
            _font.writeUnsigned32(currentRecord.getOffset());
        }

        // Alle encoding-tables wegschrijven
        Vector encodingTables = cmap.getCmapEncodingTables();
        it = encodingTables.iterator();
        TableCMAPEncoding currentTable;

        while (it.hasNext()) {
            currentTable = (TableCMAPEncoding) it.next();
            currentTable.accept(this);
        }

        _newEntry.setLength(_font.getFilePointer() - _newEntry.getOffset());
        _newEntry.setChecksum(((TableDirectory) FontManager.getInstance()
		                .searchTable("TABLEDIRECTORY")).findEntryByTag("cmap")
		                .getChecksum()); // TODO: checksum berekenen

        _entries.put(tag, _newEntry);

        padTable();
    }

    public void visitTableCMAPFormat0(TableCMAPFormat0 format0)
            throws IOException {

        _font.writeShort(format0.getFormat());
        _font.writeShort((int) format0.getLength());
        _font.writeShort((int) format0.getLanguage());

        int[] glyphIdArray = format0.getGlyphIdArray();
        for (int i = 0; i < glyphIdArray.length; i++) {
            _font.writeByte(glyphIdArray[i]);
        }
    }

    // TODO format2 mooi wegschrijven
    public void visitTableCMAPFormat2(TableCMAPFormat2 format2)
            										throws IOException {

        _font.writeShort(format2.getFormat());
        _font.writeShort((int) format2.getLength());
        _font.writeShort((int) format2.getLanguage());

        _font.write(format2.getData());
    }

    public void visitTableCMAPFormat4(TableCMAPFormat4 format4)
            										throws IOException {

        _font.writeShort(format4.getFormat());
        _font.writeShort((int) format4.getLength());
        _font.writeShort((int) format4.getLanguage());
        _font.writeShort(format4.getSegCountX2());
        _font.writeShort(format4.getSearchRange());
        _font.writeShort(format4.getEntrySelector());
        _font.writeShort(format4.getRangeShift());

        int[] endCount = format4.getEndCount();
        for (int i = 0; i < endCount.length; i++) {
            _font.writeShort(endCount[i]);
        }

        _font.writeShort(format4.getReservedPad());

        int[] startCount = format4.getStartCount();
        for (int i = 0; i < startCount.length; i++) {
            _font.writeShort(startCount[i]);
        }

        short[] idDelta = format4.getIdDelta();
        for (int i = 0; i < idDelta.length; i++) {
            _font.writeShort(idDelta[i]);
        }

        int[] idRangeOffset = format4.getIdRangeOffset();
        for (int i = 0; i < idRangeOffset.length; i++) {
            _font.writeShort(idRangeOffset[i]);
        }

        int[] glyphIdArray = format4.getGlyphIdArray();
        for (int i = 0; i < glyphIdArray.length; i++) {
            _font.writeShort(glyphIdArray[i]);
        }
    }

    public void visitTableCMAPFormat6(TableCMAPFormat6 format6)
           											throws IOException {
        _font.writeShort(format6.getFormat());
        _font.writeShort((int) format6.getLength());
        _font.writeShort((int) format6.getLanguage());
        _font.writeShort(format6.getFirstCode());
        _font.writeShort(format6.getEntryCount());

        int[] glyphIdArray = format6.getGlyphIdArray();
        for (int i = 0; i < glyphIdArray.length; i++) {
            _font.writeShort(glyphIdArray[i]);
        }
    }

    public void visitTableCMAPFormat8(TableCMAPFormat8 format8)
            										throws IOException {
        _font.writeShort(format8.getFormat());
        _font.writeShort(format8.getReserved());
        _font.writeUnsigned32(format8.getLength());
        _font.writeUnsigned32(format8.getLanguage());

        int[] is32 = format8.getIs32();
        for (int i = 0; i < is32.length; i++) {
            _font.writeByte(is32[i]);
        }

        _font.writeUnsigned32(format8.getNGroups());

        /*
         * groups wegschrijven
         */
        Vector groups = format8.getGroups();
        Iterator it = groups.iterator();
        TableCMAPGroup current;

        while (it.hasNext()) {
            current = (TableCMAPGroup) it.next();
            current.accept(this);
        }
    }

    public void visitTableCMAPFormat10(TableCMAPFormat10 format10)
           											throws IOException {
        _font.writeShort(format10.getFormat());
        _font.writeShort(format10.getReserved());
        _font.writeUnsigned32(format10.getLength());
        _font.writeUnsigned32(format10.getLanguage());

        _font.writeUnsigned32(format10.getStartCharCode());
        _font.writeUnsigned32(format10.getNumChars());

        int[] glyphs = format10.getGlyphs();
        for (int i = 0; i < glyphs.length; i++) {
            _font.writeShort(glyphs[i]);
        }
    }

    public void visitTableCMAPFormat12(TableCMAPFormat12 format12)
            										throws IOException {
        _font.writeShort(format12.getFormat());
        _font.writeShort(format12.getReserved());
        _font.writeUnsigned32(format12.getLength());
        _font.writeUnsigned32(format12.getLanguage());

        _font.writeUnsigned32(format12.getNGroups());

        /*
         * groups wegschrijven
         */
        Vector groups = format12.getGroups();
        Iterator it = groups.iterator();
        TableCMAPGroup current;

        while (it.hasNext()) {
            current = (TableCMAPGroup) it.next();
            current.accept(this);
        }
    }

    public void visitTableCMAPGroup(TableCMAPGroup group) throws IOException {
        _font.writeUnsigned32(group.getStartCharCode());
        _font.writeUnsigned32(group.getEndCharCode());
        _font.writeUnsigned32(group.getStartGlyphID());
    }

    public void visitTableGLYFSimple(TableGLYFSimple simpleGlyf)
            										throws IOException {

        /*
         * - numberOfContours/XMin/YMin/XMax/YMax wegschrijven -
         * endPointsOfContours wegschrijven - instructions wegschrijven
         */
        _font.writeShort(simpleGlyf.getNumContours());
        _font.writeShort(simpleGlyf.getXMin());
        _font.writeShort(simpleGlyf.getYMin());
        _font.writeShort(simpleGlyf.getXMax());
        _font.writeShort(simpleGlyf.getYMax());

        // voor iedere contour het eindpunt wegschrijven
        int[] endPointsOfContours = simpleGlyf.getEndPtsOfContours();

        for (int i = 0; i < endPointsOfContours.length; i++) {
            _font.writeShort(endPointsOfContours[i]);
        }

        // instructions wegschrijven
        int[] instructions = simpleGlyf.getInstructions();

        _font.writeShort(simpleGlyf.getInstructionLength());

        for (int i = 0; i < instructions.length; i++) {
            _font.writeByte(instructions[i]);
        }

        /*
         * Flags comprimeren en wegschrijven
         * 
         * Algoritme: - vorige = eerste flag, repeatvalue = 0 - FOR EACH FLAG
         * (vanaf de tweede flag)
         * 
         * IF HUIDIGE-flag = VORIGE-flag && repeat-bit(vorige) = 1 repeatValue++
         * ELSE IF repeatValue <> 0 repeat-bit van vorige flag zetten en flag
         * wegschrijven repeatValue wegschrijven ELSE flag wegschrijven
         *  
         */
        int[] flags = simpleGlyf.getFlags();
        int previous;
        int current;
        int repeatValue = 0;

        previous = flags[0];

        for (int i = 1; i < flags.length; i++) {
            current = flags[i];

            if ((previous == current)
                    && ((previous & TableGLYFSimple.REPEAT) != 0)) {
                repeatValue++;
            }
            // Als current <> previous, mogen we de flag (+ repeatValue)
            // wegschrijven
            else {
                if (repeatValue != 0) {
                    _font.writeByte(previous);
                    _font.writeByte(repeatValue);
                } else {
                    _font.writeByte(previous);
                }

                previous = current;
                repeatValue = 0;
            }
        }

        // laatste flag nog wegschrijven (previous)
        if (repeatValue != 0) {
            _font.writeByte(previous);
            _font.writeByte(repeatValue);
        } else {
            _font.writeByte(previous);
        }

        /*
         * Coordinaten wegschrijven
         * 
         * De flags (BYTE_X/Y, SAME_X/Y) voor iedere coordinaat zijn reeds
         * eerder goed gezet (zie FontManager.setGlyph() en
         * FontManager.calculateFlags() )
         */
        int[] xCoordinates = simpleGlyf.getXCoordinates();
        int[] yCoordinates = simpleGlyf.getYCoordinates();

        // X-coordinaten
        for (int i = 0; i < xCoordinates.length; i++) {
            if ((flags[i] & TableGLYFSimple.BYTE_X) == 0) {
                if ((flags[i] & TableGLYFSimple.SAME_X) == 0) {
                    _font.writeShort(xCoordinates[i]);
                }
            } else {
                if ((flags[i] & TableGLYFSimple.SAME_X) == 0) {
                    _font.writeByte(-xCoordinates[i]);
                } else {
                    _font.writeByte(xCoordinates[i]);
                }
            }
        }

        // Y-coordinaten
        for (int i = 0; i < yCoordinates.length; i++) {
            if ((flags[i] & TableGLYFSimple.BYTE_Y) == 0) {
                if ((flags[i] & TableGLYFSimple.SAME_Y) == 0) {
                    _font.writeShort(yCoordinates[i]);
                }
            } else {
                if ((flags[i] & TableGLYFSimple.SAME_Y) == 0) {
                    _font.writeByte(-yCoordinates[i]);
                } else {
                    _font.writeByte(yCoordinates[i]);
                }
            }
        }
    }

    public void visitTableGLYFComposite(TableGLYFComposite compositeGlyf)
            										throws IOException {

        // number of Contours = -1
        _font.writeShort(-1);
        _font.writeShort(compositeGlyf.getXMin());
        _font.writeShort(compositeGlyf.getYMin());
        _font.writeShort(compositeGlyf.getXMax());
        _font.writeShort(compositeGlyf.getYMax());

        /*
         * alle components wegschrijven
         */
        Vector components = compositeGlyf.getComponents();
        Iterator it = components.iterator();

        while (it.hasNext()) {
            ((GlyfComponent) it.next()).accept(this);
        }

        /*
         * instructions wegschrijven
         */
        int numInstructions = compositeGlyf.getNumInstructions();
        int[] instructions = compositeGlyf.getInstructions();

        if (numInstructions != 0) {
            _font.writeShort(numInstructions);

            for (int i = 0; i < instructions.length; i++) {
                _font.writeByte(instructions[i]);
            }
        }
    }

    public void visitTableGLYFCompositeComponent(GlyfComponent component)
            										throws IOException {

        int flags = component.getFlags();
        int glyphindex = component.getGlyphIndex();

        _font.writeShort(flags);
        _font.writeShort(glyphindex);

        if ((flags & TableGLYFComposite.ARG_1_AND_2_ARE_WORDS) != 0) {
            _font.writeShort(component.getArgument1());
            _font.writeShort(component.getArgument2());
        } else {
            _font.writeByte(component.getArgument1());
            _font.writeByte(component.getArgument2());
        }

        if ((flags & TableGLYFComposite.WE_HAVE_A_SCALE) != 0) {
            _font.writeF2dot14(component.getScale());
        } else if ((flags & TableGLYFComposite.WE_HAVE_AN_X_AND_Y_SCALE) != 0) {
            _font.writeF2dot14(component.getXScale());
            _font.writeF2dot14(component.getYScale());
        } else if ((flags & TableGLYFComposite.WE_HAVE_A_TWO_BY_TWO) != 0) {
            _font.writeF2dot14(component.getXScale());
            _font.writeF2dot14(component.getScale01());
            _font.writeF2dot14(component.getScale10());
            _font.writeF2dot14(component.getYScale());
        }
    }

    public void visitTableOther(TableOther other) throws IOException {
        String tag = new String(other.getTag());
        _newEntry = new TableDirectoryEntry();
        _newEntry.setTag(other.getTag());
        _newEntry.setOffset(_font.getFilePointer());

        _font.write(other.getData());

        _newEntry.setLength(_font.getFilePointer() - _newEntry.getOffset());
        _newEntry.setChecksum(((TableDirectory) FontManager.getInstance()
	                .searchTable("TABLEDIRECTORY")).findEntryByTag(tag)
	                .getChecksum()); // TODO: checksum berekenen

        _entries.put(new String(other.getTag()), _newEntry);

        padTable();
    }

    private void padTable() throws IOException {

        if ((_font.getFilePointer() % 4) != 0) {
            int padLength = 4 - (int) (_font.getFilePointer() % 4);
            byte[] pad = new byte[padLength];

            _font.write(pad);
        }
    }
}
/*
 * Created on 20-feb-2005
 */
package datatypes;

import java.util.*;

public class DateTime {

	private long _seconds;
	
	public DateTime(long sec) {
		_seconds = sec;
	}
	
	public long getData() {
		return _seconds;
	}
	
	public Date toDate() {
	    
	    Calendar c = Calendar.getInstance();
		c.set(1904, Calendar.JANUARY, 1, 0, 0, 0);
		
		long from1904to1970 = c.getTimeInMillis();
				
		c.setTimeInMillis((_seconds*1000) + from1904to1970);
				
		return c.getTime();  
	}
}

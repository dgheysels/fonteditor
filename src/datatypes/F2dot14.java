/*
 * Created on 20-feb-2005
 */
package datatypes;

/**
 * @author Dimitri Gheysels
 */
public class F2dot14 {
	
	private byte[] _data;
	
	public F2dot14(byte[] bytes) {
		_data = bytes;
	}
	
	public byte[] getBytes() {
		return _data;
	}
	
	public double toDouble() {
		byte  intPart    = (byte)(_data[0] >> 6);
        float fracPart   = (((_data[0] % 64) << 8) | _data[1]) / 16384;
        
        return intPart + fracPart;
	}
	
	public String toString() {
	    return String.valueOf(toDouble());
	}
}

/*
 * Created on 20-feb-2005
 */
package datatypes;

public class Fixed {
	
	private byte[] _data;
	
	public Fixed(byte[] bytes) {
		_data = bytes;
	}

	public byte[] getBytes() { return _data; };
	
	public float toFloat() {
		float intPart = ((_data[0] & 0xff) << 8) + (_data[1] & 0xff); 
		float fracPart = (((_data[2] & 0xff) << 8) + (_data[3] & 0xff)) / 65536.0f;
	    
		return intPart + fracPart;
	}
	
	public String toString() {
	    return String.valueOf(toFloat());
	}
}
